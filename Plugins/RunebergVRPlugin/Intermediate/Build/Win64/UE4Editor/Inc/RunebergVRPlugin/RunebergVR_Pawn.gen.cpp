// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "GeneratedCppIncludes.h"
#include "Private/RunebergVRPluginPrivatePCH.h"
#include "Public/RunebergVR_Pawn.h"
PRAGMA_DISABLE_OPTIMIZATION
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRunebergVR_Pawn() {}
// Cross Module References
	RUNEBERGVRPLUGIN_API UScriptStruct* Z_Construct_UScriptStruct_FGravityVariables();
	UPackage* Z_Construct_UPackage__Script_RunebergVRPlugin();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	RUNEBERGVRPLUGIN_API UFunction* Z_Construct_UFunction_ARunebergVR_Pawn_IsHMDWorn();
	RUNEBERGVRPLUGIN_API UClass* Z_Construct_UClass_ARunebergVR_Pawn();
	RUNEBERGVRPLUGIN_API UFunction* Z_Construct_UFunction_ARunebergVR_Pawn_OverridePawnValues();
	RUNEBERGVRPLUGIN_API UFunction* Z_Construct_UFunction_ARunebergVR_Pawn_PrintDebugMessage();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FColor();
	RUNEBERGVRPLUGIN_API UFunction* Z_Construct_UFunction_ARunebergVR_Pawn_RotatePawn();
	RUNEBERGVRPLUGIN_API UClass* Z_Construct_UClass_ARunebergVR_Pawn_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_APawn();
	HEADMOUNTEDDISPLAY_API UClass* Z_Construct_UClass_UMotionControllerComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UCameraComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_USceneComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UCapsuleComponent_NoRegister();
// End Cross Module References
class UScriptStruct* FGravityVariables::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern RUNEBERGVRPLUGIN_API uint32 Get_Z_Construct_UScriptStruct_FGravityVariables_CRC();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FGravityVariables, Z_Construct_UPackage__Script_RunebergVRPlugin(), TEXT("GravityVariables"), sizeof(FGravityVariables), Get_Z_Construct_UScriptStruct_FGravityVariables_CRC());
	}
	return Singleton;
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FGravityVariables(FGravityVariables::StaticStruct, TEXT("/Script/RunebergVRPlugin"), TEXT("GravityVariables"), false, nullptr, nullptr);
static struct FScriptStruct_RunebergVRPlugin_StaticRegisterNativesFGravityVariables
{
	FScriptStruct_RunebergVRPlugin_StaticRegisterNativesFGravityVariables()
	{
		UScriptStruct::DeferCppStructOps(FName(TEXT("GravityVariables")),new UScriptStruct::TCppStructOps<FGravityVariables>);
	}
} ScriptStruct_RunebergVRPlugin_StaticRegisterNativesFGravityVariables;
	UScriptStruct* Z_Construct_UScriptStruct_FGravityVariables()
	{
		UPackage* Outer = Z_Construct_UPackage__Script_RunebergVRPlugin();
		extern uint32 Get_Z_Construct_UScriptStruct_FGravityVariables_CRC();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("GravityVariables"), sizeof(FGravityVariables), Get_Z_Construct_UScriptStruct_FGravityVariables_CRC(), false);
		if (!ReturnStruct)
		{
			ReturnStruct = new(EC_InternalUseOnlyConstructor, Outer, TEXT("GravityVariables"), RF_Public|RF_Transient|RF_MarkAsNative) UScriptStruct(FObjectInitializer(), NULL, new UScriptStruct::TCppStructOps<FGravityVariables>, EStructFlags(0x00000001));
			UProperty* NewProp_GravityDirection = new(EC_InternalUseOnlyConstructor, ReturnStruct, TEXT("GravityDirection"), RF_Public|RF_Transient|RF_MarkAsNative) UStructProperty(CPP_PROPERTY_BASE(GravityDirection, FGravityVariables), 0x0010000000000001, Z_Construct_UScriptStruct_FVector());
			UProperty* NewProp_FloorTraceTolerance = new(EC_InternalUseOnlyConstructor, ReturnStruct, TEXT("FloorTraceTolerance"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(FloorTraceTolerance, FGravityVariables), 0x0010000000000001);
			UProperty* NewProp_FloorTraceRange = new(EC_InternalUseOnlyConstructor, ReturnStruct, TEXT("FloorTraceRange"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(FloorTraceRange, FGravityVariables), 0x0010000000000001);
			UProperty* NewProp_Acceleration = new(EC_InternalUseOnlyConstructor, ReturnStruct, TEXT("Acceleration"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(Acceleration, FGravityVariables), 0x0010000000000001);
			UProperty* NewProp_GravityStrength = new(EC_InternalUseOnlyConstructor, ReturnStruct, TEXT("GravityStrength"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(GravityStrength, FGravityVariables), 0x0010000000000001);
			CPP_BOOL_PROPERTY_BITMASK_STRUCT(RespondToUnevenTerrain, FGravityVariables);
			UProperty* NewProp_RespondToUnevenTerrain = new(EC_InternalUseOnlyConstructor, ReturnStruct, TEXT("RespondToUnevenTerrain"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(RespondToUnevenTerrain, FGravityVariables), 0x0010000000000001, CPP_BOOL_PROPERTY_BITMASK(RespondToUnevenTerrain, FGravityVariables), sizeof(bool), true);
			ReturnStruct->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnStruct->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnStruct, TEXT("BlueprintType"), TEXT("true"));
			MetaData->SetValue(ReturnStruct, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Pawn.h"));
			MetaData->SetValue(ReturnStruct, TEXT("ToolTip"), TEXT("Gravity settings"));
			MetaData->SetValue(NewProp_GravityDirection, TEXT("Category"), TEXT("VR"));
			MetaData->SetValue(NewProp_GravityDirection, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Pawn.h"));
			MetaData->SetValue(NewProp_GravityDirection, TEXT("ToolTip"), TEXT("Direction where this VR Pawn will \"fall\""));
			MetaData->SetValue(NewProp_FloorTraceTolerance, TEXT("Category"), TEXT("VR"));
			MetaData->SetValue(NewProp_FloorTraceTolerance, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Pawn.h"));
			MetaData->SetValue(NewProp_FloorTraceTolerance, TEXT("ToolTip"), TEXT("Minimum Z offset before terrain is considered uneven"));
			MetaData->SetValue(NewProp_FloorTraceRange, TEXT("Category"), TEXT("VR"));
			MetaData->SetValue(NewProp_FloorTraceRange, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Pawn.h"));
			MetaData->SetValue(NewProp_FloorTraceRange, TEXT("ToolTip"), TEXT("How far should the check for a floor be"));
			MetaData->SetValue(NewProp_Acceleration, TEXT("Category"), TEXT("VR"));
			MetaData->SetValue(NewProp_Acceleration, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Pawn.h"));
			MetaData->SetValue(NewProp_Acceleration, TEXT("ToolTip"), TEXT("How fast this VR Pawn will fall with gravity"));
			MetaData->SetValue(NewProp_GravityStrength, TEXT("Category"), TEXT("VR"));
			MetaData->SetValue(NewProp_GravityStrength, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Pawn.h"));
			MetaData->SetValue(NewProp_GravityStrength, TEXT("ToolTip"), TEXT("How fast this VR Pawn will fall with gravity"));
			MetaData->SetValue(NewProp_RespondToUnevenTerrain, TEXT("Category"), TEXT("VR"));
			MetaData->SetValue(NewProp_RespondToUnevenTerrain, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Pawn.h"));
			MetaData->SetValue(NewProp_RespondToUnevenTerrain, TEXT("ToolTip"), TEXT("Respond to uneven terrain - Gravity MUST also be enabled"));
#endif
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FGravityVariables_CRC() { return 2816896633U; }
	void ARunebergVR_Pawn::StaticRegisterNativesARunebergVR_Pawn()
	{
		UClass* Class = ARunebergVR_Pawn::StaticClass();
		static const TNameNativePtrPair<ANSICHAR> AnsiFuncs[] = {
			{ "IsHMDWorn", (Native)&ARunebergVR_Pawn::execIsHMDWorn },
			{ "OverridePawnValues", (Native)&ARunebergVR_Pawn::execOverridePawnValues },
			{ "PrintDebugMessage", (Native)&ARunebergVR_Pawn::execPrintDebugMessage },
			{ "RotatePawn", (Native)&ARunebergVR_Pawn::execRotatePawn },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, AnsiFuncs, ARRAY_COUNT(AnsiFuncs));
	}
	UFunction* Z_Construct_UFunction_ARunebergVR_Pawn_IsHMDWorn()
	{
		struct RunebergVR_Pawn_eventIsHMDWorn_Parms
		{
			bool ReturnValue;
		};
		UObject* Outer = Z_Construct_UClass_ARunebergVR_Pawn();
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("IsHMDWorn"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), nullptr, (EFunctionFlags)0x04020401, 65535, sizeof(RunebergVR_Pawn_eventIsHMDWorn_Parms));
			CPP_BOOL_PROPERTY_BITMASK_STRUCT(ReturnValue, RunebergVR_Pawn_eventIsHMDWorn_Parms);
			UProperty* NewProp_ReturnValue = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("ReturnValue"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(ReturnValue, RunebergVR_Pawn_eventIsHMDWorn_Parms), 0x0010000000000580, CPP_BOOL_PROPERTY_BITMASK(ReturnValue, RunebergVR_Pawn_eventIsHMDWorn_Parms), sizeof(bool), true);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("VR"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Pawn.h"));
			MetaData->SetValue(ReturnFunction, TEXT("ToolTip"), TEXT("Check if HMD is worn"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_ARunebergVR_Pawn_OverridePawnValues()
	{
		struct RunebergVR_Pawn_eventOverridePawnValues_Parms
		{
			float PawnBaseEyeHeight;
			float FOV;
			float CapsuleHalfHeight;
			float CapsuleRadius;
			FVector CapsuleRelativeLocation;
			FVector SceneLocation;
			FVector LeftControllerLocation;
			FVector RightControllerLocation;
		};
		UObject* Outer = Z_Construct_UClass_ARunebergVR_Pawn();
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("OverridePawnValues"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), nullptr, (EFunctionFlags)0x04820401, 65535, sizeof(RunebergVR_Pawn_eventOverridePawnValues_Parms));
			UProperty* NewProp_RightControllerLocation = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("RightControllerLocation"), RF_Public|RF_Transient|RF_MarkAsNative) UStructProperty(CPP_PROPERTY_BASE(RightControllerLocation, RunebergVR_Pawn_eventOverridePawnValues_Parms), 0x0010000000000080, Z_Construct_UScriptStruct_FVector());
			UProperty* NewProp_LeftControllerLocation = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("LeftControllerLocation"), RF_Public|RF_Transient|RF_MarkAsNative) UStructProperty(CPP_PROPERTY_BASE(LeftControllerLocation, RunebergVR_Pawn_eventOverridePawnValues_Parms), 0x0010000000000080, Z_Construct_UScriptStruct_FVector());
			UProperty* NewProp_SceneLocation = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("SceneLocation"), RF_Public|RF_Transient|RF_MarkAsNative) UStructProperty(CPP_PROPERTY_BASE(SceneLocation, RunebergVR_Pawn_eventOverridePawnValues_Parms), 0x0010000000000080, Z_Construct_UScriptStruct_FVector());
			UProperty* NewProp_CapsuleRelativeLocation = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("CapsuleRelativeLocation"), RF_Public|RF_Transient|RF_MarkAsNative) UStructProperty(CPP_PROPERTY_BASE(CapsuleRelativeLocation, RunebergVR_Pawn_eventOverridePawnValues_Parms), 0x0010000000000080, Z_Construct_UScriptStruct_FVector());
			UProperty* NewProp_CapsuleRadius = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("CapsuleRadius"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(CapsuleRadius, RunebergVR_Pawn_eventOverridePawnValues_Parms), 0x0010000000000080);
			UProperty* NewProp_CapsuleHalfHeight = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("CapsuleHalfHeight"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(CapsuleHalfHeight, RunebergVR_Pawn_eventOverridePawnValues_Parms), 0x0010000000000080);
			UProperty* NewProp_FOV = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("FOV"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(FOV, RunebergVR_Pawn_eventOverridePawnValues_Parms), 0x0010000000000080);
			UProperty* NewProp_PawnBaseEyeHeight = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("PawnBaseEyeHeight"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(PawnBaseEyeHeight, RunebergVR_Pawn_eventOverridePawnValues_Parms), 0x0010000000000080);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("VR"));
			MetaData->SetValue(ReturnFunction, TEXT("CPP_Default_CapsuleHalfHeight"), TEXT("96.000000"));
			MetaData->SetValue(ReturnFunction, TEXT("CPP_Default_CapsuleRadius"), TEXT("22.000000"));
			MetaData->SetValue(ReturnFunction, TEXT("CPP_Default_CapsuleRelativeLocation"), TEXT("0.000000,0.000000,-110.000000"));
			MetaData->SetValue(ReturnFunction, TEXT("CPP_Default_FOV"), TEXT("110.000000"));
			MetaData->SetValue(ReturnFunction, TEXT("CPP_Default_LeftControllerLocation"), TEXT("0.000000,0.000000,110.000000"));
			MetaData->SetValue(ReturnFunction, TEXT("CPP_Default_PawnBaseEyeHeight"), TEXT("0.000000"));
			MetaData->SetValue(ReturnFunction, TEXT("CPP_Default_RightControllerLocation"), TEXT("0.000000,0.000000,110.000000"));
			MetaData->SetValue(ReturnFunction, TEXT("CPP_Default_SceneLocation"), TEXT("0.000000,0.000000,-110.000000"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Pawn.h"));
			MetaData->SetValue(ReturnFunction, TEXT("ToolTip"), TEXT("Override default pawn vr values"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_ARunebergVR_Pawn_PrintDebugMessage()
	{
		struct RunebergVR_Pawn_eventPrintDebugMessage_Parms
		{
			FString Message;
			bool OverwriteExisting;
			float Duration;
			FColor Color;
		};
		UObject* Outer = Z_Construct_UClass_ARunebergVR_Pawn();
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("PrintDebugMessage"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), nullptr, (EFunctionFlags)0x04820401, 65535, sizeof(RunebergVR_Pawn_eventPrintDebugMessage_Parms));
			UProperty* NewProp_Color = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("Color"), RF_Public|RF_Transient|RF_MarkAsNative) UStructProperty(CPP_PROPERTY_BASE(Color, RunebergVR_Pawn_eventPrintDebugMessage_Parms), 0x0010000000000080, Z_Construct_UScriptStruct_FColor());
			UProperty* NewProp_Duration = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("Duration"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(Duration, RunebergVR_Pawn_eventPrintDebugMessage_Parms), 0x0010000000000080);
			CPP_BOOL_PROPERTY_BITMASK_STRUCT(OverwriteExisting, RunebergVR_Pawn_eventPrintDebugMessage_Parms);
			UProperty* NewProp_OverwriteExisting = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("OverwriteExisting"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(OverwriteExisting, RunebergVR_Pawn_eventPrintDebugMessage_Parms), 0x0010000000000080, CPP_BOOL_PROPERTY_BITMASK(OverwriteExisting, RunebergVR_Pawn_eventPrintDebugMessage_Parms), sizeof(bool), true);
			UProperty* NewProp_Message = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("Message"), RF_Public|RF_Transient|RF_MarkAsNative) UStrProperty(CPP_PROPERTY_BASE(Message, RunebergVR_Pawn_eventPrintDebugMessage_Parms), 0x0010000000000080);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("VR"));
			MetaData->SetValue(ReturnFunction, TEXT("CPP_Default_Color"), TEXT("(R=255,G=0,B=0,A=255)"));
			MetaData->SetValue(ReturnFunction, TEXT("CPP_Default_Duration"), TEXT("5.000000"));
			MetaData->SetValue(ReturnFunction, TEXT("CPP_Default_OverwriteExisting"), TEXT("false"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Pawn.h"));
			MetaData->SetValue(ReturnFunction, TEXT("ToolTip"), TEXT("Print debug message"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_ARunebergVR_Pawn_RotatePawn()
	{
		struct RunebergVR_Pawn_eventRotatePawn_Parms
		{
			float RotationRate;
			float XAxisInput;
			float YAxisInput;
		};
		UObject* Outer = Z_Construct_UClass_ARunebergVR_Pawn();
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("RotatePawn"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), nullptr, (EFunctionFlags)0x04020401, 65535, sizeof(RunebergVR_Pawn_eventRotatePawn_Parms));
			UProperty* NewProp_YAxisInput = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("YAxisInput"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(YAxisInput, RunebergVR_Pawn_eventRotatePawn_Parms), 0x0010000000000080);
			UProperty* NewProp_XAxisInput = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("XAxisInput"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(XAxisInput, RunebergVR_Pawn_eventRotatePawn_Parms), 0x0010000000000080);
			UProperty* NewProp_RotationRate = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("RotationRate"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(RotationRate, RunebergVR_Pawn_eventRotatePawn_Parms), 0x0010000000000080);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("VR"));
			MetaData->SetValue(ReturnFunction, TEXT("CPP_Default_RotationRate"), TEXT("1.000000"));
			MetaData->SetValue(ReturnFunction, TEXT("CPP_Default_XAxisInput"), TEXT("0.000000"));
			MetaData->SetValue(ReturnFunction, TEXT("CPP_Default_YAxisInput"), TEXT("0.000000"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Pawn.h"));
			MetaData->SetValue(ReturnFunction, TEXT("ToolTip"), TEXT("Pawn Rotation - usefull for static mouse rotation during development"));
#endif
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_ARunebergVR_Pawn_NoRegister()
	{
		return ARunebergVR_Pawn::StaticClass();
	}
	UClass* Z_Construct_UClass_ARunebergVR_Pawn()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_APawn();
			Z_Construct_UPackage__Script_RunebergVRPlugin();
			OuterClass = ARunebergVR_Pawn::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= (EClassFlags)0x20900080u;

				OuterClass->LinkChild(Z_Construct_UFunction_ARunebergVR_Pawn_IsHMDWorn());
				OuterClass->LinkChild(Z_Construct_UFunction_ARunebergVR_Pawn_OverridePawnValues());
				OuterClass->LinkChild(Z_Construct_UFunction_ARunebergVR_Pawn_PrintDebugMessage());
				OuterClass->LinkChild(Z_Construct_UFunction_ARunebergVR_Pawn_RotatePawn());

				UProperty* NewProp_MotionController_Right = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("MotionController_Right"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(MotionController_Right, ARunebergVR_Pawn), 0x00100000000a001d, Z_Construct_UClass_UMotionControllerComponent_NoRegister());
				UProperty* NewProp_MotionController_Left = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("MotionController_Left"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(MotionController_Left, ARunebergVR_Pawn), 0x00100000000a001d, Z_Construct_UClass_UMotionControllerComponent_NoRegister());
				UProperty* NewProp_Camera = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("Camera"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(Camera, ARunebergVR_Pawn), 0x00100000000a001d, Z_Construct_UClass_UCameraComponent_NoRegister());
				UProperty* NewProp_Scene = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("Scene"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(Scene, ARunebergVR_Pawn), 0x00100000000a001d, Z_Construct_UClass_USceneComponent_NoRegister());
				UProperty* NewProp_CapsuleCollision = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("CapsuleCollision"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(CapsuleCollision, ARunebergVR_Pawn), 0x00100000000a001d, Z_Construct_UClass_UCapsuleComponent_NoRegister());
				UProperty* NewProp_HMDLocationOffset = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("HMDLocationOffset"), RF_Public|RF_Transient|RF_MarkAsNative) UStructProperty(CPP_PROPERTY_BASE(HMDLocationOffset, ARunebergVR_Pawn), 0x0010000000000005, Z_Construct_UScriptStruct_FVector());
				UProperty* NewProp_OculusLocationOffset = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("OculusLocationOffset"), RF_Public|RF_Transient|RF_MarkAsNative) UStructProperty(CPP_PROPERTY_BASE(OculusLocationOffset, ARunebergVR_Pawn), 0x0010000000000005, Z_Construct_UScriptStruct_FVector());
				UProperty* NewProp_StepUpRate = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("StepUpRate"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(StepUpRate, ARunebergVR_Pawn), 0x0010000000000005);
				UProperty* NewProp_GravityVariables = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("GravityVariables"), RF_Public|RF_Transient|RF_MarkAsNative) UStructProperty(CPP_PROPERTY_BASE(GravityVariables, ARunebergVR_Pawn), 0x0010000000000005, Z_Construct_UScriptStruct_FGravityVariables());
				CPP_BOOL_PROPERTY_BITMASK_STRUCT(EnableGravity, ARunebergVR_Pawn);
				UProperty* NewProp_EnableGravity = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("EnableGravity"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(EnableGravity, ARunebergVR_Pawn), 0x0010000000000005, CPP_BOOL_PROPERTY_BITMASK(EnableGravity, ARunebergVR_Pawn), sizeof(bool), true);
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_ARunebergVR_Pawn_IsHMDWorn(), "IsHMDWorn"); // 2961334651
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_ARunebergVR_Pawn_OverridePawnValues(), "OverridePawnValues"); // 3650645330
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_ARunebergVR_Pawn_PrintDebugMessage(), "PrintDebugMessage"); // 1113914660
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_ARunebergVR_Pawn_RotatePawn(), "RotatePawn"); // 2843101076
				static TCppClassTypeInfo<TCppClassTypeTraits<ARunebergVR_Pawn> > StaticCppClassTypeInfo;
				OuterClass->SetCppTypeInfo(&StaticCppClassTypeInfo);
				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("HideCategories"), TEXT("Navigation"));
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("RunebergVR_Pawn.h"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Pawn.h"));
				MetaData->SetValue(OuterClass, TEXT("ObjectInitializerConstructorDeclared"), TEXT(""));
				MetaData->SetValue(NewProp_MotionController_Right, TEXT("Category"), TEXT("VR"));
				MetaData->SetValue(NewProp_MotionController_Right, TEXT("EditInline"), TEXT("true"));
				MetaData->SetValue(NewProp_MotionController_Right, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Pawn.h"));
				MetaData->SetValue(NewProp_MotionController_Left, TEXT("Category"), TEXT("VR"));
				MetaData->SetValue(NewProp_MotionController_Left, TEXT("EditInline"), TEXT("true"));
				MetaData->SetValue(NewProp_MotionController_Left, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Pawn.h"));
				MetaData->SetValue(NewProp_MotionController_Left, TEXT("ToolTip"), TEXT("VR Motion Controllers"));
				MetaData->SetValue(NewProp_Camera, TEXT("Category"), TEXT("VR"));
				MetaData->SetValue(NewProp_Camera, TEXT("EditInline"), TEXT("true"));
				MetaData->SetValue(NewProp_Camera, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Pawn.h"));
				MetaData->SetValue(NewProp_Camera, TEXT("ToolTip"), TEXT("Pawn camera"));
				MetaData->SetValue(NewProp_Scene, TEXT("Category"), TEXT("VR"));
				MetaData->SetValue(NewProp_Scene, TEXT("EditInline"), TEXT("true"));
				MetaData->SetValue(NewProp_Scene, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Pawn.h"));
				MetaData->SetValue(NewProp_Scene, TEXT("ToolTip"), TEXT("Scene Component (for headset posiitoning)"));
				MetaData->SetValue(NewProp_CapsuleCollision, TEXT("Category"), TEXT("VR"));
				MetaData->SetValue(NewProp_CapsuleCollision, TEXT("EditInline"), TEXT("true"));
				MetaData->SetValue(NewProp_CapsuleCollision, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Pawn.h"));
				MetaData->SetValue(NewProp_CapsuleCollision, TEXT("ToolTip"), TEXT("Capsule Component"));
				MetaData->SetValue(NewProp_HMDLocationOffset, TEXT("Category"), TEXT("VR"));
				MetaData->SetValue(NewProp_HMDLocationOffset, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Pawn.h"));
				MetaData->SetValue(NewProp_HMDLocationOffset, TEXT("ToolTip"), TEXT("HMD Location Offset"));
				MetaData->SetValue(NewProp_OculusLocationOffset, TEXT("Category"), TEXT("VR"));
				MetaData->SetValue(NewProp_OculusLocationOffset, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Pawn.h"));
				MetaData->SetValue(NewProp_OculusLocationOffset, TEXT("ToolTip"), TEXT("Oculus HMD Location Offset"));
				MetaData->SetValue(NewProp_StepUpRate, TEXT("Category"), TEXT("VR"));
				MetaData->SetValue(NewProp_StepUpRate, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Pawn.h"));
				MetaData->SetValue(NewProp_StepUpRate, TEXT("ToolTip"), TEXT("Uneven Terrain Step Up rate"));
				MetaData->SetValue(NewProp_GravityVariables, TEXT("Category"), TEXT("VR"));
				MetaData->SetValue(NewProp_GravityVariables, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Pawn.h"));
				MetaData->SetValue(NewProp_GravityVariables, TEXT("ToolTip"), TEXT("Gravity variables"));
				MetaData->SetValue(NewProp_EnableGravity, TEXT("Category"), TEXT("VR"));
				MetaData->SetValue(NewProp_EnableGravity, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Pawn.h"));
				MetaData->SetValue(NewProp_EnableGravity, TEXT("ToolTip"), TEXT("Enable gravity for this pawn"));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	IMPLEMENT_CLASS(ARunebergVR_Pawn, 3904429630);
	static FCompiledInDefer Z_CompiledInDefer_UClass_ARunebergVR_Pawn(Z_Construct_UClass_ARunebergVR_Pawn, &ARunebergVR_Pawn::StaticClass, TEXT("/Script/RunebergVRPlugin"), TEXT("ARunebergVR_Pawn"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ARunebergVR_Pawn);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
PRAGMA_ENABLE_OPTIMIZATION
