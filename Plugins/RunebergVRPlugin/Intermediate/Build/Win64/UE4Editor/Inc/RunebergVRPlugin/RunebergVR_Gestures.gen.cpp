// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "GeneratedCppIncludes.h"
#include "Private/RunebergVRPluginPrivatePCH.h"
#include "Public/RunebergVR_Gestures.h"
PRAGMA_DISABLE_OPTIMIZATION
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRunebergVR_Gestures() {}
// Cross Module References
	RUNEBERGVRPLUGIN_API UScriptStruct* Z_Construct_UScriptStruct_FDrawnGestures();
	UPackage* Z_Construct_UPackage__Script_RunebergVRPlugin();
	ENGINE_API UClass* Z_Construct_UClass_USplineMeshComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_USplineComponent_NoRegister();
	RUNEBERGVRPLUGIN_API UScriptStruct* Z_Construct_UScriptStruct_FIntArray();
	RUNEBERGVRPLUGIN_API UScriptStruct* Z_Construct_UScriptStruct_FFloatArray();
	RUNEBERGVRPLUGIN_API UFunction* Z_Construct_UFunction_URunebergVR_Gestures_DrawVRGesture();
	RUNEBERGVRPLUGIN_API UClass* Z_Construct_UClass_URunebergVR_Gestures();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FRotator();
	ENGINE_API UClass* Z_Construct_UClass_UMaterial_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UStaticMesh_NoRegister();
	RUNEBERGVRPLUGIN_API UScriptStruct* Z_Construct_UScriptStruct_FVRGesture();
	RUNEBERGVRPLUGIN_API UFunction* Z_Construct_UFunction_URunebergVR_Gestures_EmptyKnownGestures();
	RUNEBERGVRPLUGIN_API UFunction* Z_Construct_UFunction_URunebergVR_Gestures_FindGesture();
	RUNEBERGVRPLUGIN_API UFunction* Z_Construct_UFunction_URunebergVR_Gestures_SaveGestureToDB();
	RUNEBERGVRPLUGIN_API UFunction* Z_Construct_UFunction_URunebergVR_Gestures_StartRecordingGesture();
	RUNEBERGVRPLUGIN_API UFunction* Z_Construct_UFunction_URunebergVR_Gestures_StopRecordingGesture();
	RUNEBERGVRPLUGIN_API UClass* Z_Construct_UClass_URunebergVR_Gestures_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_USceneComponent();
	RUNEBERGVRPLUGIN_API UClass* Z_Construct_UClass_URunebergVR_Gestures_Database_NoRegister();
// End Cross Module References
class UScriptStruct* FDrawnGestures::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern RUNEBERGVRPLUGIN_API uint32 Get_Z_Construct_UScriptStruct_FDrawnGestures_CRC();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDrawnGestures, Z_Construct_UPackage__Script_RunebergVRPlugin(), TEXT("DrawnGestures"), sizeof(FDrawnGestures), Get_Z_Construct_UScriptStruct_FDrawnGestures_CRC());
	}
	return Singleton;
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDrawnGestures(FDrawnGestures::StaticStruct, TEXT("/Script/RunebergVRPlugin"), TEXT("DrawnGestures"), false, nullptr, nullptr);
static struct FScriptStruct_RunebergVRPlugin_StaticRegisterNativesFDrawnGestures
{
	FScriptStruct_RunebergVRPlugin_StaticRegisterNativesFDrawnGestures()
	{
		UScriptStruct::DeferCppStructOps(FName(TEXT("DrawnGestures")),new UScriptStruct::TCppStructOps<FDrawnGestures>);
	}
} ScriptStruct_RunebergVRPlugin_StaticRegisterNativesFDrawnGestures;
	UScriptStruct* Z_Construct_UScriptStruct_FDrawnGestures()
	{
		UPackage* Outer = Z_Construct_UPackage__Script_RunebergVRPlugin();
		extern uint32 Get_Z_Construct_UScriptStruct_FDrawnGestures_CRC();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DrawnGestures"), sizeof(FDrawnGestures), Get_Z_Construct_UScriptStruct_FDrawnGestures_CRC(), false);
		if (!ReturnStruct)
		{
			ReturnStruct = new(EC_InternalUseOnlyConstructor, Outer, TEXT("DrawnGestures"), RF_Public|RF_Transient|RF_MarkAsNative) UScriptStruct(FObjectInitializer(), NULL, new UScriptStruct::TCppStructOps<FDrawnGestures>, EStructFlags(0x00000005));
			UProperty* NewProp_SplineMesh = new(EC_InternalUseOnlyConstructor, ReturnStruct, TEXT("SplineMesh"), RF_Public|RF_Transient|RF_MarkAsNative) UArrayProperty(CPP_PROPERTY_BASE(SplineMesh, FDrawnGestures), 0x0010008000000009);
			UProperty* NewProp_SplineMesh_Inner = new(EC_InternalUseOnlyConstructor, NewProp_SplineMesh, TEXT("SplineMesh"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(FObjectInitializer(), EC_CppProperty, 0, 0x0000000000080008, Z_Construct_UClass_USplineMeshComponent_NoRegister());
			UProperty* NewProp_SplineComponent = new(EC_InternalUseOnlyConstructor, ReturnStruct, TEXT("SplineComponent"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(SplineComponent, FDrawnGestures), 0x0010000000080009, Z_Construct_UClass_USplineComponent_NoRegister());
			ReturnStruct->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnStruct->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnStruct, TEXT("BlueprintType"), TEXT("true"));
			MetaData->SetValue(ReturnStruct, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Gestures.h"));
			MetaData->SetValue(NewProp_SplineMesh, TEXT("Category"), TEXT("VR"));
			MetaData->SetValue(NewProp_SplineMesh, TEXT("EditInline"), TEXT("true"));
			MetaData->SetValue(NewProp_SplineMesh, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Gestures.h"));
			MetaData->SetValue(NewProp_SplineComponent, TEXT("Category"), TEXT("VR"));
			MetaData->SetValue(NewProp_SplineComponent, TEXT("EditInline"), TEXT("true"));
			MetaData->SetValue(NewProp_SplineComponent, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Gestures.h"));
#endif
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDrawnGestures_CRC() { return 2230304511U; }
class UScriptStruct* FIntArray::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern RUNEBERGVRPLUGIN_API uint32 Get_Z_Construct_UScriptStruct_FIntArray_CRC();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FIntArray, Z_Construct_UPackage__Script_RunebergVRPlugin(), TEXT("IntArray"), sizeof(FIntArray), Get_Z_Construct_UScriptStruct_FIntArray_CRC());
	}
	return Singleton;
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FIntArray(FIntArray::StaticStruct, TEXT("/Script/RunebergVRPlugin"), TEXT("IntArray"), false, nullptr, nullptr);
static struct FScriptStruct_RunebergVRPlugin_StaticRegisterNativesFIntArray
{
	FScriptStruct_RunebergVRPlugin_StaticRegisterNativesFIntArray()
	{
		UScriptStruct::DeferCppStructOps(FName(TEXT("IntArray")),new UScriptStruct::TCppStructOps<FIntArray>);
	}
} ScriptStruct_RunebergVRPlugin_StaticRegisterNativesFIntArray;
	UScriptStruct* Z_Construct_UScriptStruct_FIntArray()
	{
		UPackage* Outer = Z_Construct_UPackage__Script_RunebergVRPlugin();
		extern uint32 Get_Z_Construct_UScriptStruct_FIntArray_CRC();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("IntArray"), sizeof(FIntArray), Get_Z_Construct_UScriptStruct_FIntArray_CRC(), false);
		if (!ReturnStruct)
		{
			ReturnStruct = new(EC_InternalUseOnlyConstructor, Outer, TEXT("IntArray"), RF_Public|RF_Transient|RF_MarkAsNative) UScriptStruct(FObjectInitializer(), NULL, new UScriptStruct::TCppStructOps<FIntArray>, EStructFlags(0x00000001));
			UProperty* NewProp_IntValue = new(EC_InternalUseOnlyConstructor, ReturnStruct, TEXT("IntValue"), RF_Public|RF_Transient|RF_MarkAsNative) UArrayProperty(CPP_PROPERTY_BASE(IntValue, FIntArray), 0x0010000000000001);
			UProperty* NewProp_IntValue_Inner = new(EC_InternalUseOnlyConstructor, NewProp_IntValue, TEXT("IntValue"), RF_Public|RF_Transient|RF_MarkAsNative) UIntProperty(FObjectInitializer(), EC_CppProperty, 0, 0x0000000000000000);
			ReturnStruct->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnStruct->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnStruct, TEXT("BlueprintType"), TEXT("true"));
			MetaData->SetValue(ReturnStruct, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Gestures.h"));
			MetaData->SetValue(NewProp_IntValue, TEXT("Category"), TEXT("VR"));
			MetaData->SetValue(NewProp_IntValue, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Gestures.h"));
#endif
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FIntArray_CRC() { return 549613621U; }
class UScriptStruct* FFloatArray::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern RUNEBERGVRPLUGIN_API uint32 Get_Z_Construct_UScriptStruct_FFloatArray_CRC();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FFloatArray, Z_Construct_UPackage__Script_RunebergVRPlugin(), TEXT("FloatArray"), sizeof(FFloatArray), Get_Z_Construct_UScriptStruct_FFloatArray_CRC());
	}
	return Singleton;
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FFloatArray(FFloatArray::StaticStruct, TEXT("/Script/RunebergVRPlugin"), TEXT("FloatArray"), false, nullptr, nullptr);
static struct FScriptStruct_RunebergVRPlugin_StaticRegisterNativesFFloatArray
{
	FScriptStruct_RunebergVRPlugin_StaticRegisterNativesFFloatArray()
	{
		UScriptStruct::DeferCppStructOps(FName(TEXT("FloatArray")),new UScriptStruct::TCppStructOps<FFloatArray>);
	}
} ScriptStruct_RunebergVRPlugin_StaticRegisterNativesFFloatArray;
	UScriptStruct* Z_Construct_UScriptStruct_FFloatArray()
	{
		UPackage* Outer = Z_Construct_UPackage__Script_RunebergVRPlugin();
		extern uint32 Get_Z_Construct_UScriptStruct_FFloatArray_CRC();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("FloatArray"), sizeof(FFloatArray), Get_Z_Construct_UScriptStruct_FFloatArray_CRC(), false);
		if (!ReturnStruct)
		{
			ReturnStruct = new(EC_InternalUseOnlyConstructor, Outer, TEXT("FloatArray"), RF_Public|RF_Transient|RF_MarkAsNative) UScriptStruct(FObjectInitializer(), NULL, new UScriptStruct::TCppStructOps<FFloatArray>, EStructFlags(0x00000001));
			UProperty* NewProp_FloatValue = new(EC_InternalUseOnlyConstructor, ReturnStruct, TEXT("FloatValue"), RF_Public|RF_Transient|RF_MarkAsNative) UArrayProperty(CPP_PROPERTY_BASE(FloatValue, FFloatArray), 0x0010000000000001);
			UProperty* NewProp_FloatValue_Inner = new(EC_InternalUseOnlyConstructor, NewProp_FloatValue, TEXT("FloatValue"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(FObjectInitializer(), EC_CppProperty, 0, 0x0000000000000000);
			ReturnStruct->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnStruct->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnStruct, TEXT("BlueprintType"), TEXT("true"));
			MetaData->SetValue(ReturnStruct, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Gestures.h"));
			MetaData->SetValue(NewProp_FloatValue, TEXT("Category"), TEXT("VR"));
			MetaData->SetValue(NewProp_FloatValue, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Gestures.h"));
#endif
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FFloatArray_CRC() { return 3460206054U; }
	void URunebergVR_Gestures::StaticRegisterNativesURunebergVR_Gestures()
	{
		UClass* Class = URunebergVR_Gestures::StaticClass();
		static const TNameNativePtrPair<ANSICHAR> AnsiFuncs[] = {
			{ "DrawVRGesture", (Native)&URunebergVR_Gestures::execDrawVRGesture },
			{ "EmptyKnownGestures", (Native)&URunebergVR_Gestures::execEmptyKnownGestures },
			{ "FindGesture", (Native)&URunebergVR_Gestures::execFindGesture },
			{ "SaveGestureToDB", (Native)&URunebergVR_Gestures::execSaveGestureToDB },
			{ "StartRecordingGesture", (Native)&URunebergVR_Gestures::execStartRecordingGesture },
			{ "StopRecordingGesture", (Native)&URunebergVR_Gestures::execStopRecordingGesture },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, AnsiFuncs, ARRAY_COUNT(AnsiFuncs));
	}
	UFunction* Z_Construct_UFunction_URunebergVR_Gestures_DrawVRGesture()
	{
		struct RunebergVR_Gestures_eventDrawVRGesture_Parms
		{
			FVRGesture VR_Gesture;
			UStaticMesh* LineMesh;
			UMaterial* LineMaterial;
			FVector OriginLocation;
			FRotator OriginRotation;
			FVector OffsetLocation;
			float OffsetDistance;
			float Lifetime;
		};
		UObject* Outer = Z_Construct_UClass_URunebergVR_Gestures();
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("DrawVRGesture"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), nullptr, (EFunctionFlags)0x04820401, 65535, sizeof(RunebergVR_Gestures_eventDrawVRGesture_Parms));
			UProperty* NewProp_Lifetime = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("Lifetime"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(Lifetime, RunebergVR_Gestures_eventDrawVRGesture_Parms), 0x0010000000000080);
			UProperty* NewProp_OffsetDistance = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("OffsetDistance"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(OffsetDistance, RunebergVR_Gestures_eventDrawVRGesture_Parms), 0x0010000000000080);
			UProperty* NewProp_OffsetLocation = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("OffsetLocation"), RF_Public|RF_Transient|RF_MarkAsNative) UStructProperty(CPP_PROPERTY_BASE(OffsetLocation, RunebergVR_Gestures_eventDrawVRGesture_Parms), 0x0010000000000080, Z_Construct_UScriptStruct_FVector());
			UProperty* NewProp_OriginRotation = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("OriginRotation"), RF_Public|RF_Transient|RF_MarkAsNative) UStructProperty(CPP_PROPERTY_BASE(OriginRotation, RunebergVR_Gestures_eventDrawVRGesture_Parms), 0x0010000000000080, Z_Construct_UScriptStruct_FRotator());
			UProperty* NewProp_OriginLocation = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("OriginLocation"), RF_Public|RF_Transient|RF_MarkAsNative) UStructProperty(CPP_PROPERTY_BASE(OriginLocation, RunebergVR_Gestures_eventDrawVRGesture_Parms), 0x0010000000000080, Z_Construct_UScriptStruct_FVector());
			UProperty* NewProp_LineMaterial = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("LineMaterial"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(LineMaterial, RunebergVR_Gestures_eventDrawVRGesture_Parms), 0x0010000000000080, Z_Construct_UClass_UMaterial_NoRegister());
			UProperty* NewProp_LineMesh = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("LineMesh"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(LineMesh, RunebergVR_Gestures_eventDrawVRGesture_Parms), 0x0010000000000080, Z_Construct_UClass_UStaticMesh_NoRegister());
			UProperty* NewProp_VR_Gesture = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("VR_Gesture"), RF_Public|RF_Transient|RF_MarkAsNative) UStructProperty(CPP_PROPERTY_BASE(VR_Gesture, RunebergVR_Gestures_eventDrawVRGesture_Parms), 0x0010000000000080, Z_Construct_UScriptStruct_FVRGesture());
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("VR"));
			MetaData->SetValue(ReturnFunction, TEXT("CPP_Default_Lifetime"), TEXT("3.000000"));
			MetaData->SetValue(ReturnFunction, TEXT("CPP_Default_OffsetDistance"), TEXT("100.000000"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Gestures.h"));
			MetaData->SetValue(ReturnFunction, TEXT("ToolTip"), TEXT("Draw stored gesture"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_URunebergVR_Gestures_EmptyKnownGestures()
	{
		struct RunebergVR_Gestures_eventEmptyKnownGestures_Parms
		{
			bool ReturnValue;
		};
		UObject* Outer = Z_Construct_UClass_URunebergVR_Gestures();
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("EmptyKnownGestures"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), nullptr, (EFunctionFlags)0x04020401, 65535, sizeof(RunebergVR_Gestures_eventEmptyKnownGestures_Parms));
			CPP_BOOL_PROPERTY_BITMASK_STRUCT(ReturnValue, RunebergVR_Gestures_eventEmptyKnownGestures_Parms);
			UProperty* NewProp_ReturnValue = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("ReturnValue"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(ReturnValue, RunebergVR_Gestures_eventEmptyKnownGestures_Parms), 0x0010000000000580, CPP_BOOL_PROPERTY_BITMASK(ReturnValue, RunebergVR_Gestures_eventEmptyKnownGestures_Parms), sizeof(bool), true);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("VR"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Gestures.h"));
			MetaData->SetValue(ReturnFunction, TEXT("ToolTip"), TEXT("Convenience function to empty a Gesture DB"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_URunebergVR_Gestures_FindGesture()
	{
		struct RunebergVR_Gestures_eventFindGesture_Parms
		{
			FString ReturnValue;
		};
		UObject* Outer = Z_Construct_UClass_URunebergVR_Gestures();
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("FindGesture"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), nullptr, (EFunctionFlags)0x04020401, 65535, sizeof(RunebergVR_Gestures_eventFindGesture_Parms));
			UProperty* NewProp_ReturnValue = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("ReturnValue"), RF_Public|RF_Transient|RF_MarkAsNative) UStrProperty(CPP_PROPERTY_BASE(ReturnValue, RunebergVR_Gestures_eventFindGesture_Parms), 0x0010000000000580);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("VR"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Gestures.h"));
			MetaData->SetValue(ReturnFunction, TEXT("ToolTip"), TEXT("Find Gesture in the Known Gestures Database"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_URunebergVR_Gestures_SaveGestureToDB()
	{
		struct RunebergVR_Gestures_eventSaveGestureToDB_Parms
		{
			bool ReturnValue;
		};
		UObject* Outer = Z_Construct_UClass_URunebergVR_Gestures();
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("SaveGestureToDB"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), nullptr, (EFunctionFlags)0x04020401, 65535, sizeof(RunebergVR_Gestures_eventSaveGestureToDB_Parms));
			CPP_BOOL_PROPERTY_BITMASK_STRUCT(ReturnValue, RunebergVR_Gestures_eventSaveGestureToDB_Parms);
			UProperty* NewProp_ReturnValue = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("ReturnValue"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(ReturnValue, RunebergVR_Gestures_eventSaveGestureToDB_Parms), 0x0010000000000580, CPP_BOOL_PROPERTY_BITMASK(ReturnValue, RunebergVR_Gestures_eventSaveGestureToDB_Parms), sizeof(bool), true);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("VR"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Gestures.h"));
			MetaData->SetValue(ReturnFunction, TEXT("ToolTip"), TEXT("Convenience function to save a gesture to DB"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_URunebergVR_Gestures_StartRecordingGesture()
	{
		struct RunebergVR_Gestures_eventStartRecordingGesture_Parms
		{
			float RecordingInterval;
			FString GestureName;
			bool DrawLine;
			UStaticMesh* LineMesh;
			UMaterial* LineMaterial;
			FVector LineOffset;
			FRotator RotationOffset;
		};
		UObject* Outer = Z_Construct_UClass_URunebergVR_Gestures();
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("StartRecordingGesture"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), nullptr, (EFunctionFlags)0x04820401, 65535, sizeof(RunebergVR_Gestures_eventStartRecordingGesture_Parms));
			UProperty* NewProp_RotationOffset = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("RotationOffset"), RF_Public|RF_Transient|RF_MarkAsNative) UStructProperty(CPP_PROPERTY_BASE(RotationOffset, RunebergVR_Gestures_eventStartRecordingGesture_Parms), 0x0010000000000080, Z_Construct_UScriptStruct_FRotator());
			UProperty* NewProp_LineOffset = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("LineOffset"), RF_Public|RF_Transient|RF_MarkAsNative) UStructProperty(CPP_PROPERTY_BASE(LineOffset, RunebergVR_Gestures_eventStartRecordingGesture_Parms), 0x0010000000000080, Z_Construct_UScriptStruct_FVector());
			UProperty* NewProp_LineMaterial = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("LineMaterial"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(LineMaterial, RunebergVR_Gestures_eventStartRecordingGesture_Parms), 0x0010000000000080, Z_Construct_UClass_UMaterial_NoRegister());
			UProperty* NewProp_LineMesh = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("LineMesh"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(LineMesh, RunebergVR_Gestures_eventStartRecordingGesture_Parms), 0x0010000000000080, Z_Construct_UClass_UStaticMesh_NoRegister());
			CPP_BOOL_PROPERTY_BITMASK_STRUCT(DrawLine, RunebergVR_Gestures_eventStartRecordingGesture_Parms);
			UProperty* NewProp_DrawLine = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("DrawLine"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(DrawLine, RunebergVR_Gestures_eventStartRecordingGesture_Parms), 0x0010000000000080, CPP_BOOL_PROPERTY_BITMASK(DrawLine, RunebergVR_Gestures_eventStartRecordingGesture_Parms), sizeof(bool), true);
			UProperty* NewProp_GestureName = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("GestureName"), RF_Public|RF_Transient|RF_MarkAsNative) UStrProperty(CPP_PROPERTY_BASE(GestureName, RunebergVR_Gestures_eventStartRecordingGesture_Parms), 0x0010000000000080);
			UProperty* NewProp_RecordingInterval = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("RecordingInterval"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(RecordingInterval, RunebergVR_Gestures_eventStartRecordingGesture_Parms), 0x0010000000000080);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("VR"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Gestures.h"));
			MetaData->SetValue(ReturnFunction, TEXT("ToolTip"), TEXT("Start recording VR Gesture"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_URunebergVR_Gestures_StopRecordingGesture()
	{
		struct RunebergVR_Gestures_eventStopRecordingGesture_Parms
		{
			bool SaveToDB;
			FVRGesture ReturnValue;
		};
		UObject* Outer = Z_Construct_UClass_URunebergVR_Gestures();
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("StopRecordingGesture"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), nullptr, (EFunctionFlags)0x04020401, 65535, sizeof(RunebergVR_Gestures_eventStopRecordingGesture_Parms));
			UProperty* NewProp_ReturnValue = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("ReturnValue"), RF_Public|RF_Transient|RF_MarkAsNative) UStructProperty(CPP_PROPERTY_BASE(ReturnValue, RunebergVR_Gestures_eventStopRecordingGesture_Parms), 0x0010000000000580, Z_Construct_UScriptStruct_FVRGesture());
			CPP_BOOL_PROPERTY_BITMASK_STRUCT(SaveToDB, RunebergVR_Gestures_eventStopRecordingGesture_Parms);
			UProperty* NewProp_SaveToDB = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("SaveToDB"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(SaveToDB, RunebergVR_Gestures_eventStopRecordingGesture_Parms), 0x0010000000000080, CPP_BOOL_PROPERTY_BITMASK(SaveToDB, RunebergVR_Gestures_eventStopRecordingGesture_Parms), sizeof(bool), true);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("VR"));
			MetaData->SetValue(ReturnFunction, TEXT("CPP_Default_SaveToDB"), TEXT("false"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Gestures.h"));
			MetaData->SetValue(ReturnFunction, TEXT("ToolTip"), TEXT("Stop recording VR Gesture"));
#endif
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_URunebergVR_Gestures_NoRegister()
	{
		return URunebergVR_Gestures::StaticClass();
	}
	UClass* Z_Construct_UClass_URunebergVR_Gestures()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_USceneComponent();
			Z_Construct_UPackage__Script_RunebergVRPlugin();
			OuterClass = URunebergVR_Gestures::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= (EClassFlags)0x20B00080u;

				OuterClass->LinkChild(Z_Construct_UFunction_URunebergVR_Gestures_DrawVRGesture());
				OuterClass->LinkChild(Z_Construct_UFunction_URunebergVR_Gestures_EmptyKnownGestures());
				OuterClass->LinkChild(Z_Construct_UFunction_URunebergVR_Gestures_FindGesture());
				OuterClass->LinkChild(Z_Construct_UFunction_URunebergVR_Gestures_SaveGestureToDB());
				OuterClass->LinkChild(Z_Construct_UFunction_URunebergVR_Gestures_StartRecordingGesture());
				OuterClass->LinkChild(Z_Construct_UFunction_URunebergVR_Gestures_StopRecordingGesture());

				CPP_BOOL_PROPERTY_BITMASK_STRUCT(IsRecording, URunebergVR_Gestures);
				UProperty* NewProp_IsRecording = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("IsRecording"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(IsRecording, URunebergVR_Gestures), 0x0010000000020005, CPP_BOOL_PROPERTY_BITMASK(IsRecording, URunebergVR_Gestures), sizeof(bool), true);
				UProperty* NewProp_VRGesture = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("VRGesture"), RF_Public|RF_Transient|RF_MarkAsNative) UStructProperty(CPP_PROPERTY_BASE(VRGesture, URunebergVR_Gestures), 0x0010000000020005, Z_Construct_UScriptStruct_FVRGesture());
				UProperty* NewProp_MaxSlope = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("MaxSlope"), RF_Public|RF_Transient|RF_MarkAsNative) UUnsizedIntProperty(CPP_PROPERTY_BASE(MaxSlope, URunebergVR_Gestures), 0x0010000000000005);
				UProperty* NewProp_VectorThreshold = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("VectorThreshold"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(VectorThreshold, URunebergVR_Gestures), 0x0010000000000005);
				UProperty* NewProp_GlobalThreshold = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("GlobalThreshold"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(GlobalThreshold, URunebergVR_Gestures), 0x0010000000000005);
				UProperty* NewProp_KnownGesturesDB = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("KnownGesturesDB"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(KnownGesturesDB, URunebergVR_Gestures), 0x0010000000000005, Z_Construct_UClass_URunebergVR_Gestures_Database_NoRegister());
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_URunebergVR_Gestures_DrawVRGesture(), "DrawVRGesture"); // 1414036728
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_URunebergVR_Gestures_EmptyKnownGestures(), "EmptyKnownGestures"); // 4101467776
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_URunebergVR_Gestures_FindGesture(), "FindGesture"); // 1716234688
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_URunebergVR_Gestures_SaveGestureToDB(), "SaveGestureToDB"); // 1355107640
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_URunebergVR_Gestures_StartRecordingGesture(), "StartRecordingGesture"); // 465392400
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_URunebergVR_Gestures_StopRecordingGesture(), "StopRecordingGesture"); // 3954259694
				static TCppClassTypeInfo<TCppClassTypeTraits<URunebergVR_Gestures> > StaticCppClassTypeInfo;
				OuterClass->SetCppTypeInfo(&StaticCppClassTypeInfo);
				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("BlueprintSpawnableComponent"), TEXT(""));
				MetaData->SetValue(OuterClass, TEXT("ClassGroupNames"), TEXT("VR"));
				MetaData->SetValue(OuterClass, TEXT("HideCategories"), TEXT("Trigger PhysicsVolume"));
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("RunebergVR_Gestures.h"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Gestures.h"));
				MetaData->SetValue(NewProp_IsRecording, TEXT("Category"), TEXT("VR - Read Only"));
				MetaData->SetValue(NewProp_IsRecording, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Gestures.h"));
				MetaData->SetValue(NewProp_IsRecording, TEXT("ToolTip"), TEXT("Wether or not this Gestures Component is currently recording"));
				MetaData->SetValue(NewProp_VRGesture, TEXT("Category"), TEXT("VR - Read Only"));
				MetaData->SetValue(NewProp_VRGesture, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Gestures.h"));
				MetaData->SetValue(NewProp_VRGesture, TEXT("ToolTip"), TEXT("Latest recorded VR Gesture"));
				MetaData->SetValue(NewProp_MaxSlope, TEXT("Category"), TEXT("VR"));
				MetaData->SetValue(NewProp_MaxSlope, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Gestures.h"));
				MetaData->SetValue(NewProp_MaxSlope, TEXT("ToolTip"), TEXT("Maximum number of vertical or horizontal steps in a row for the DTW similarity test"));
				MetaData->SetValue(NewProp_VectorThreshold, TEXT("Category"), TEXT("VR"));
				MetaData->SetValue(NewProp_VectorThreshold, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Gestures.h"));
				MetaData->SetValue(NewProp_VectorThreshold, TEXT("ToolTip"), TEXT("How far apart each point can be for the DTW similarity test measured in DTW distance units"));
				MetaData->SetValue(NewProp_GlobalThreshold, TEXT("Category"), TEXT("VR"));
				MetaData->SetValue(NewProp_GlobalThreshold, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Gestures.h"));
				MetaData->SetValue(NewProp_GlobalThreshold, TEXT("ToolTip"), TEXT("How different two gesture patterns can be for testing similarity measured in DTW distance units"));
				MetaData->SetValue(NewProp_KnownGesturesDB, TEXT("Category"), TEXT("VR"));
				MetaData->SetValue(NewProp_KnownGesturesDB, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Gestures.h"));
				MetaData->SetValue(NewProp_KnownGesturesDB, TEXT("ToolTip"), TEXT("Known gestures database to be used by this component"));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	IMPLEMENT_CLASS(URunebergVR_Gestures, 4168343214);
	static FCompiledInDefer Z_CompiledInDefer_UClass_URunebergVR_Gestures(Z_Construct_UClass_URunebergVR_Gestures, &URunebergVR_Gestures::StaticClass, TEXT("/Script/RunebergVRPlugin"), TEXT("URunebergVR_Gestures"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(URunebergVR_Gestures);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
PRAGMA_ENABLE_OPTIMIZATION
