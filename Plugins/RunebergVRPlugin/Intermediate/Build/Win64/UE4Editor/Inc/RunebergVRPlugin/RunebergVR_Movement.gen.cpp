// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "GeneratedCppIncludes.h"
#include "Private/RunebergVRPluginPrivatePCH.h"
#include "Public/RunebergVR_Movement.h"
PRAGMA_DISABLE_OPTIMIZATION
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRunebergVR_Movement() {}
// Cross Module References
	RUNEBERGVRPLUGIN_API UFunction* Z_Construct_UFunction_URunebergVR_Movement_ApplySpeedMultiplier();
	RUNEBERGVRPLUGIN_API UClass* Z_Construct_UClass_URunebergVR_Movement();
	RUNEBERGVRPLUGIN_API UFunction* Z_Construct_UFunction_URunebergVR_Movement_BounceBackFromVRBounds();
	RUNEBERGVRPLUGIN_API UFunction* Z_Construct_UFunction_URunebergVR_Movement_DisableVRMovement();
	RUNEBERGVRPLUGIN_API UFunction* Z_Construct_UFunction_URunebergVR_Movement_Enable360Movement();
	ENGINE_API UClass* Z_Construct_UClass_USceneComponent_NoRegister();
	RUNEBERGVRPLUGIN_API UFunction* Z_Construct_UFunction_URunebergVR_Movement_EnableVRMovement();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FRotator();
	RUNEBERGVRPLUGIN_API UFunction* Z_Construct_UFunction_URunebergVR_Movement_TimedDashMove();
	RUNEBERGVRPLUGIN_API UFunction* Z_Construct_UFunction_URunebergVR_Movement_TimedMovement();
	RUNEBERGVRPLUGIN_API UClass* Z_Construct_UClass_URunebergVR_Movement_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UActorComponent();
	UPackage* Z_Construct_UPackage__Script_RunebergVRPlugin();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
// End Cross Module References
	void URunebergVR_Movement::StaticRegisterNativesURunebergVR_Movement()
	{
		UClass* Class = URunebergVR_Movement::StaticClass();
		static const TNameNativePtrPair<ANSICHAR> AnsiFuncs[] = {
			{ "ApplySpeedMultiplier", (Native)&URunebergVR_Movement::execApplySpeedMultiplier },
			{ "BounceBackFromVRBounds", (Native)&URunebergVR_Movement::execBounceBackFromVRBounds },
			{ "DisableVRMovement", (Native)&URunebergVR_Movement::execDisableVRMovement },
			{ "Enable360Movement", (Native)&URunebergVR_Movement::execEnable360Movement },
			{ "EnableVRMovement", (Native)&URunebergVR_Movement::execEnableVRMovement },
			{ "TimedDashMove", (Native)&URunebergVR_Movement::execTimedDashMove },
			{ "TimedMovement", (Native)&URunebergVR_Movement::execTimedMovement },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, AnsiFuncs, ARRAY_COUNT(AnsiFuncs));
	}
	UFunction* Z_Construct_UFunction_URunebergVR_Movement_ApplySpeedMultiplier()
	{
		struct RunebergVR_Movement_eventApplySpeedMultiplier_Parms
		{
			float SpeedMultiplier;
			float BaseSpeed;
			bool UseCurrentSpeedAsBase;
		};
		UObject* Outer = Z_Construct_UClass_URunebergVR_Movement();
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("ApplySpeedMultiplier"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), nullptr, (EFunctionFlags)0x04020401, 65535, sizeof(RunebergVR_Movement_eventApplySpeedMultiplier_Parms));
			CPP_BOOL_PROPERTY_BITMASK_STRUCT(UseCurrentSpeedAsBase, RunebergVR_Movement_eventApplySpeedMultiplier_Parms);
			UProperty* NewProp_UseCurrentSpeedAsBase = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("UseCurrentSpeedAsBase"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(UseCurrentSpeedAsBase, RunebergVR_Movement_eventApplySpeedMultiplier_Parms), 0x0010000000000080, CPP_BOOL_PROPERTY_BITMASK(UseCurrentSpeedAsBase, RunebergVR_Movement_eventApplySpeedMultiplier_Parms), sizeof(bool), true);
			UProperty* NewProp_BaseSpeed = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("BaseSpeed"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(BaseSpeed, RunebergVR_Movement_eventApplySpeedMultiplier_Parms), 0x0010000000000080);
			UProperty* NewProp_SpeedMultiplier = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("SpeedMultiplier"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(SpeedMultiplier, RunebergVR_Movement_eventApplySpeedMultiplier_Parms), 0x0010000000000080);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("VR"));
			MetaData->SetValue(ReturnFunction, TEXT("CPP_Default_BaseSpeed"), TEXT("3.000000"));
			MetaData->SetValue(ReturnFunction, TEXT("CPP_Default_SpeedMultiplier"), TEXT("2.000000"));
			MetaData->SetValue(ReturnFunction, TEXT("CPP_Default_UseCurrentSpeedAsBase"), TEXT("false"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Movement.h"));
			MetaData->SetValue(ReturnFunction, TEXT("ToolTip"), TEXT("Apply acceleration multiplier to current movement speed - can be used for smooth acceleration / deceleration"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_URunebergVR_Movement_BounceBackFromVRBounds()
	{
		struct RunebergVR_Movement_eventBounceBackFromVRBounds_Parms
		{
			float MovementSpeed;
			float MovementDuration;
			bool ResetMovementStateAfterBounce;
		};
		UObject* Outer = Z_Construct_UClass_URunebergVR_Movement();
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("BounceBackFromVRBounds"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), nullptr, (EFunctionFlags)0x04020401, 65535, sizeof(RunebergVR_Movement_eventBounceBackFromVRBounds_Parms));
			CPP_BOOL_PROPERTY_BITMASK_STRUCT(ResetMovementStateAfterBounce, RunebergVR_Movement_eventBounceBackFromVRBounds_Parms);
			UProperty* NewProp_ResetMovementStateAfterBounce = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("ResetMovementStateAfterBounce"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(ResetMovementStateAfterBounce, RunebergVR_Movement_eventBounceBackFromVRBounds_Parms), 0x0010000000000080, CPP_BOOL_PROPERTY_BITMASK(ResetMovementStateAfterBounce, RunebergVR_Movement_eventBounceBackFromVRBounds_Parms), sizeof(bool), true);
			UProperty* NewProp_MovementDuration = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("MovementDuration"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(MovementDuration, RunebergVR_Movement_eventBounceBackFromVRBounds_Parms), 0x0010000000000080);
			UProperty* NewProp_MovementSpeed = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("MovementSpeed"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(MovementSpeed, RunebergVR_Movement_eventBounceBackFromVRBounds_Parms), 0x0010000000000080);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("VR"));
			MetaData->SetValue(ReturnFunction, TEXT("CPP_Default_MovementDuration"), TEXT("0.500000"));
			MetaData->SetValue(ReturnFunction, TEXT("CPP_Default_MovementSpeed"), TEXT("3.000000"));
			MetaData->SetValue(ReturnFunction, TEXT("CPP_Default_ResetMovementStateAfterBounce"), TEXT("false"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Movement.h"));
			MetaData->SetValue(ReturnFunction, TEXT("ToolTip"), TEXT("Bounce back from VR bounds"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_URunebergVR_Movement_DisableVRMovement()
	{
		UObject* Outer = Z_Construct_UClass_URunebergVR_Movement();
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("DisableVRMovement"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), nullptr, (EFunctionFlags)0x04020401, 65535);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("VR"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Movement.h"));
			MetaData->SetValue(ReturnFunction, TEXT("ToolTip"), TEXT("Disable VR Movement"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_URunebergVR_Movement_Enable360Movement()
	{
		struct RunebergVR_Movement_eventEnable360Movement_Parms
		{
			USceneComponent* MovementDirectionReference;
			bool LockPitch;
			bool LockYaw;
			bool LockRoll;
			float MovementSpeed;
			float XAxisInput;
			float YAxisInput;
		};
		UObject* Outer = Z_Construct_UClass_URunebergVR_Movement();
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("Enable360Movement"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), nullptr, (EFunctionFlags)0x04020401, 65535, sizeof(RunebergVR_Movement_eventEnable360Movement_Parms));
			UProperty* NewProp_YAxisInput = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("YAxisInput"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(YAxisInput, RunebergVR_Movement_eventEnable360Movement_Parms), 0x0010000000000080);
			UProperty* NewProp_XAxisInput = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("XAxisInput"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(XAxisInput, RunebergVR_Movement_eventEnable360Movement_Parms), 0x0010000000000080);
			UProperty* NewProp_MovementSpeed = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("MovementSpeed"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(MovementSpeed, RunebergVR_Movement_eventEnable360Movement_Parms), 0x0010000000000080);
			CPP_BOOL_PROPERTY_BITMASK_STRUCT(LockRoll, RunebergVR_Movement_eventEnable360Movement_Parms);
			UProperty* NewProp_LockRoll = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("LockRoll"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(LockRoll, RunebergVR_Movement_eventEnable360Movement_Parms), 0x0010000000000080, CPP_BOOL_PROPERTY_BITMASK(LockRoll, RunebergVR_Movement_eventEnable360Movement_Parms), sizeof(bool), true);
			CPP_BOOL_PROPERTY_BITMASK_STRUCT(LockYaw, RunebergVR_Movement_eventEnable360Movement_Parms);
			UProperty* NewProp_LockYaw = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("LockYaw"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(LockYaw, RunebergVR_Movement_eventEnable360Movement_Parms), 0x0010000000000080, CPP_BOOL_PROPERTY_BITMASK(LockYaw, RunebergVR_Movement_eventEnable360Movement_Parms), sizeof(bool), true);
			CPP_BOOL_PROPERTY_BITMASK_STRUCT(LockPitch, RunebergVR_Movement_eventEnable360Movement_Parms);
			UProperty* NewProp_LockPitch = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("LockPitch"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(LockPitch, RunebergVR_Movement_eventEnable360Movement_Parms), 0x0010000000000080, CPP_BOOL_PROPERTY_BITMASK(LockPitch, RunebergVR_Movement_eventEnable360Movement_Parms), sizeof(bool), true);
			UProperty* NewProp_MovementDirectionReference = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("MovementDirectionReference"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(MovementDirectionReference, RunebergVR_Movement_eventEnable360Movement_Parms), 0x0010000000080080, Z_Construct_UClass_USceneComponent_NoRegister());
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("VR"));
			MetaData->SetValue(ReturnFunction, TEXT("CPP_Default_LockPitch"), TEXT("false"));
			MetaData->SetValue(ReturnFunction, TEXT("CPP_Default_LockRoll"), TEXT("false"));
			MetaData->SetValue(ReturnFunction, TEXT("CPP_Default_LockYaw"), TEXT("false"));
			MetaData->SetValue(ReturnFunction, TEXT("CPP_Default_MovementSpeed"), TEXT("3.000000"));
			MetaData->SetValue(ReturnFunction, TEXT("CPP_Default_XAxisInput"), TEXT("0.000000"));
			MetaData->SetValue(ReturnFunction, TEXT("CPP_Default_YAxisInput"), TEXT("0.000000"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Movement.h"));
			MetaData->SetValue(ReturnFunction, TEXT("ToolTip"), TEXT("Full 360 Movement"));
			MetaData->SetValue(NewProp_MovementDirectionReference, TEXT("EditInline"), TEXT("true"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_URunebergVR_Movement_EnableVRMovement()
	{
		struct RunebergVR_Movement_eventEnableVRMovement_Parms
		{
			float MovementSpeed;
			USceneComponent* MovementDirectionReference;
			bool ObeyNavMesh;
			bool LockPitch;
			bool LockYaw;
			bool LockRoll;
			FRotator CustomDirection;
		};
		UObject* Outer = Z_Construct_UClass_URunebergVR_Movement();
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("EnableVRMovement"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), nullptr, (EFunctionFlags)0x04820401, 65535, sizeof(RunebergVR_Movement_eventEnableVRMovement_Parms));
			UProperty* NewProp_CustomDirection = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("CustomDirection"), RF_Public|RF_Transient|RF_MarkAsNative) UStructProperty(CPP_PROPERTY_BASE(CustomDirection, RunebergVR_Movement_eventEnableVRMovement_Parms), 0x0010000000000080, Z_Construct_UScriptStruct_FRotator());
			CPP_BOOL_PROPERTY_BITMASK_STRUCT(LockRoll, RunebergVR_Movement_eventEnableVRMovement_Parms);
			UProperty* NewProp_LockRoll = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("LockRoll"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(LockRoll, RunebergVR_Movement_eventEnableVRMovement_Parms), 0x0010000000000080, CPP_BOOL_PROPERTY_BITMASK(LockRoll, RunebergVR_Movement_eventEnableVRMovement_Parms), sizeof(bool), true);
			CPP_BOOL_PROPERTY_BITMASK_STRUCT(LockYaw, RunebergVR_Movement_eventEnableVRMovement_Parms);
			UProperty* NewProp_LockYaw = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("LockYaw"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(LockYaw, RunebergVR_Movement_eventEnableVRMovement_Parms), 0x0010000000000080, CPP_BOOL_PROPERTY_BITMASK(LockYaw, RunebergVR_Movement_eventEnableVRMovement_Parms), sizeof(bool), true);
			CPP_BOOL_PROPERTY_BITMASK_STRUCT(LockPitch, RunebergVR_Movement_eventEnableVRMovement_Parms);
			UProperty* NewProp_LockPitch = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("LockPitch"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(LockPitch, RunebergVR_Movement_eventEnableVRMovement_Parms), 0x0010000000000080, CPP_BOOL_PROPERTY_BITMASK(LockPitch, RunebergVR_Movement_eventEnableVRMovement_Parms), sizeof(bool), true);
			CPP_BOOL_PROPERTY_BITMASK_STRUCT(ObeyNavMesh, RunebergVR_Movement_eventEnableVRMovement_Parms);
			UProperty* NewProp_ObeyNavMesh = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("ObeyNavMesh"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(ObeyNavMesh, RunebergVR_Movement_eventEnableVRMovement_Parms), 0x0010000000000080, CPP_BOOL_PROPERTY_BITMASK(ObeyNavMesh, RunebergVR_Movement_eventEnableVRMovement_Parms), sizeof(bool), true);
			UProperty* NewProp_MovementDirectionReference = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("MovementDirectionReference"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(MovementDirectionReference, RunebergVR_Movement_eventEnableVRMovement_Parms), 0x0010000000080080, Z_Construct_UClass_USceneComponent_NoRegister());
			UProperty* NewProp_MovementSpeed = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("MovementSpeed"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(MovementSpeed, RunebergVR_Movement_eventEnableVRMovement_Parms), 0x0010000000000080);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("VR"));
			MetaData->SetValue(ReturnFunction, TEXT("CPP_Default_LockPitch"), TEXT("false"));
			MetaData->SetValue(ReturnFunction, TEXT("CPP_Default_LockRoll"), TEXT("false"));
			MetaData->SetValue(ReturnFunction, TEXT("CPP_Default_LockYaw"), TEXT("false"));
			MetaData->SetValue(ReturnFunction, TEXT("CPP_Default_MovementSpeed"), TEXT("3.000000"));
			MetaData->SetValue(ReturnFunction, TEXT("CPP_Default_ObeyNavMesh"), TEXT("false"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Movement.h"));
			MetaData->SetValue(ReturnFunction, TEXT("ToolTip"), TEXT("Enable VR Movement"));
			MetaData->SetValue(NewProp_MovementDirectionReference, TEXT("EditInline"), TEXT("true"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_URunebergVR_Movement_TimedDashMove()
	{
		struct RunebergVR_Movement_eventTimedDashMove_Parms
		{
			float MovementDuration;
			float MovementSpeed;
			FRotator MovementDirection;
			bool ObeyNavMesh;
		};
		UObject* Outer = Z_Construct_UClass_URunebergVR_Movement();
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("TimedDashMove"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), nullptr, (EFunctionFlags)0x04820401, 65535, sizeof(RunebergVR_Movement_eventTimedDashMove_Parms));
			CPP_BOOL_PROPERTY_BITMASK_STRUCT(ObeyNavMesh, RunebergVR_Movement_eventTimedDashMove_Parms);
			UProperty* NewProp_ObeyNavMesh = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("ObeyNavMesh"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(ObeyNavMesh, RunebergVR_Movement_eventTimedDashMove_Parms), 0x0010000000000080, CPP_BOOL_PROPERTY_BITMASK(ObeyNavMesh, RunebergVR_Movement_eventTimedDashMove_Parms), sizeof(bool), true);
			UProperty* NewProp_MovementDirection = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("MovementDirection"), RF_Public|RF_Transient|RF_MarkAsNative) UStructProperty(CPP_PROPERTY_BASE(MovementDirection, RunebergVR_Movement_eventTimedDashMove_Parms), 0x0010000000000080, Z_Construct_UScriptStruct_FRotator());
			UProperty* NewProp_MovementSpeed = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("MovementSpeed"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(MovementSpeed, RunebergVR_Movement_eventTimedDashMove_Parms), 0x0010000000000080);
			UProperty* NewProp_MovementDuration = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("MovementDuration"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(MovementDuration, RunebergVR_Movement_eventTimedDashMove_Parms), 0x0010000000000080);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("VR"));
			MetaData->SetValue(ReturnFunction, TEXT("CPP_Default_MovementDuration"), TEXT("5.000000"));
			MetaData->SetValue(ReturnFunction, TEXT("CPP_Default_MovementSpeed"), TEXT("3.000000"));
			MetaData->SetValue(ReturnFunction, TEXT("CPP_Default_ObeyNavMesh"), TEXT("false"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Movement.h"));
			MetaData->SetValue(ReturnFunction, TEXT("ToolTip"), TEXT("Dash move (timed)  - dash into a predefined direction and time"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_URunebergVR_Movement_TimedMovement()
	{
		struct RunebergVR_Movement_eventTimedMovement_Parms
		{
			float MovementDuration;
			float MovementSpeed;
			USceneComponent* MovementDirectionReference;
			bool LockPitchY;
			bool LockYawZ;
			bool LockRollX;
			FRotator CustomDirection;
			bool ObeyNavMesh;
		};
		UObject* Outer = Z_Construct_UClass_URunebergVR_Movement();
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("TimedMovement"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), nullptr, (EFunctionFlags)0x04820401, 65535, sizeof(RunebergVR_Movement_eventTimedMovement_Parms));
			CPP_BOOL_PROPERTY_BITMASK_STRUCT(ObeyNavMesh, RunebergVR_Movement_eventTimedMovement_Parms);
			UProperty* NewProp_ObeyNavMesh = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("ObeyNavMesh"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(ObeyNavMesh, RunebergVR_Movement_eventTimedMovement_Parms), 0x0010000000000080, CPP_BOOL_PROPERTY_BITMASK(ObeyNavMesh, RunebergVR_Movement_eventTimedMovement_Parms), sizeof(bool), true);
			UProperty* NewProp_CustomDirection = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("CustomDirection"), RF_Public|RF_Transient|RF_MarkAsNative) UStructProperty(CPP_PROPERTY_BASE(CustomDirection, RunebergVR_Movement_eventTimedMovement_Parms), 0x0010000000000080, Z_Construct_UScriptStruct_FRotator());
			CPP_BOOL_PROPERTY_BITMASK_STRUCT(LockRollX, RunebergVR_Movement_eventTimedMovement_Parms);
			UProperty* NewProp_LockRollX = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("LockRollX"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(LockRollX, RunebergVR_Movement_eventTimedMovement_Parms), 0x0010000000000080, CPP_BOOL_PROPERTY_BITMASK(LockRollX, RunebergVR_Movement_eventTimedMovement_Parms), sizeof(bool), true);
			CPP_BOOL_PROPERTY_BITMASK_STRUCT(LockYawZ, RunebergVR_Movement_eventTimedMovement_Parms);
			UProperty* NewProp_LockYawZ = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("LockYawZ"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(LockYawZ, RunebergVR_Movement_eventTimedMovement_Parms), 0x0010000000000080, CPP_BOOL_PROPERTY_BITMASK(LockYawZ, RunebergVR_Movement_eventTimedMovement_Parms), sizeof(bool), true);
			CPP_BOOL_PROPERTY_BITMASK_STRUCT(LockPitchY, RunebergVR_Movement_eventTimedMovement_Parms);
			UProperty* NewProp_LockPitchY = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("LockPitchY"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(LockPitchY, RunebergVR_Movement_eventTimedMovement_Parms), 0x0010000000000080, CPP_BOOL_PROPERTY_BITMASK(LockPitchY, RunebergVR_Movement_eventTimedMovement_Parms), sizeof(bool), true);
			UProperty* NewProp_MovementDirectionReference = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("MovementDirectionReference"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(MovementDirectionReference, RunebergVR_Movement_eventTimedMovement_Parms), 0x0010000000080080, Z_Construct_UClass_USceneComponent_NoRegister());
			UProperty* NewProp_MovementSpeed = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("MovementSpeed"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(MovementSpeed, RunebergVR_Movement_eventTimedMovement_Parms), 0x0010000000000080);
			UProperty* NewProp_MovementDuration = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("MovementDuration"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(MovementDuration, RunebergVR_Movement_eventTimedMovement_Parms), 0x0010000000000080);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("VR"));
			MetaData->SetValue(ReturnFunction, TEXT("CPP_Default_LockPitchY"), TEXT("false"));
			MetaData->SetValue(ReturnFunction, TEXT("CPP_Default_LockRollX"), TEXT("false"));
			MetaData->SetValue(ReturnFunction, TEXT("CPP_Default_LockYawZ"), TEXT("false"));
			MetaData->SetValue(ReturnFunction, TEXT("CPP_Default_MovementDuration"), TEXT("5.000000"));
			MetaData->SetValue(ReturnFunction, TEXT("CPP_Default_MovementSpeed"), TEXT("3.000000"));
			MetaData->SetValue(ReturnFunction, TEXT("CPP_Default_ObeyNavMesh"), TEXT("false"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Movement.h"));
			MetaData->SetValue(ReturnFunction, TEXT("ToolTip"), TEXT("Timed movement - move pawn for a specified amount of time"));
			MetaData->SetValue(NewProp_MovementDirectionReference, TEXT("EditInline"), TEXT("true"));
#endif
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_URunebergVR_Movement_NoRegister()
	{
		return URunebergVR_Movement::StaticClass();
	}
	UClass* Z_Construct_UClass_URunebergVR_Movement()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_UActorComponent();
			Z_Construct_UPackage__Script_RunebergVRPlugin();
			OuterClass = URunebergVR_Movement::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= (EClassFlags)0x20B00080u;

				OuterClass->LinkChild(Z_Construct_UFunction_URunebergVR_Movement_ApplySpeedMultiplier());
				OuterClass->LinkChild(Z_Construct_UFunction_URunebergVR_Movement_BounceBackFromVRBounds());
				OuterClass->LinkChild(Z_Construct_UFunction_URunebergVR_Movement_DisableVRMovement());
				OuterClass->LinkChild(Z_Construct_UFunction_URunebergVR_Movement_Enable360Movement());
				OuterClass->LinkChild(Z_Construct_UFunction_URunebergVR_Movement_EnableVRMovement());
				OuterClass->LinkChild(Z_Construct_UFunction_URunebergVR_Movement_TimedDashMove());
				OuterClass->LinkChild(Z_Construct_UFunction_URunebergVR_Movement_TimedMovement());

				CPP_BOOL_PROPERTY_BITMASK_STRUCT(EnableTerrainCheck, URunebergVR_Movement);
				UProperty* NewProp_EnableTerrainCheck = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("EnableTerrainCheck"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(EnableTerrainCheck, URunebergVR_Movement), 0x0010000000000005, CPP_BOOL_PROPERTY_BITMASK(EnableTerrainCheck, URunebergVR_Movement), sizeof(bool), true);
				CPP_BOOL_PROPERTY_BITMASK_STRUCT(IsBouncingBackFromVRBounds, URunebergVR_Movement);
				UProperty* NewProp_IsBouncingBackFromVRBounds = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("IsBouncingBackFromVRBounds"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(IsBouncingBackFromVRBounds, URunebergVR_Movement), 0x0010000000000005, CPP_BOOL_PROPERTY_BITMASK(IsBouncingBackFromVRBounds, URunebergVR_Movement), sizeof(bool), true);
				CPP_BOOL_PROPERTY_BITMASK_STRUCT(IsMoving, URunebergVR_Movement);
				UProperty* NewProp_IsMoving = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("IsMoving"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(IsMoving, URunebergVR_Movement), 0x0010000000000005, CPP_BOOL_PROPERTY_BITMASK(IsMoving, URunebergVR_Movement), sizeof(bool), true);
				UProperty* NewProp_NavMeshTolerance = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("NavMeshTolerance"), RF_Public|RF_Transient|RF_MarkAsNative) UStructProperty(CPP_PROPERTY_BASE(NavMeshTolerance, URunebergVR_Movement), 0x0010000000000005, Z_Construct_UScriptStruct_FVector());
				UProperty* NewProp_CurrentMovementDirectionReference = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("CurrentMovementDirectionReference"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(CurrentMovementDirectionReference, URunebergVR_Movement), 0x001000000008000d, Z_Construct_UClass_USceneComponent_NoRegister());
				UProperty* NewProp_OffsetRotation = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("OffsetRotation"), RF_Public|RF_Transient|RF_MarkAsNative) UStructProperty(CPP_PROPERTY_BASE(OffsetRotation, URunebergVR_Movement), 0x0010000000000005, Z_Construct_UScriptStruct_FRotator());
				UProperty* NewProp_TargetRotation = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("TargetRotation"), RF_Public|RF_Transient|RF_MarkAsNative) UStructProperty(CPP_PROPERTY_BASE(TargetRotation, URunebergVR_Movement), 0x0010000000000005, Z_Construct_UScriptStruct_FRotator());
				UProperty* NewProp_CurrentMovementSpeed = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("CurrentMovementSpeed"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(CurrentMovementSpeed, URunebergVR_Movement), 0x0010000000000005);
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_URunebergVR_Movement_ApplySpeedMultiplier(), "ApplySpeedMultiplier"); // 2003274995
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_URunebergVR_Movement_BounceBackFromVRBounds(), "BounceBackFromVRBounds"); // 2269777498
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_URunebergVR_Movement_DisableVRMovement(), "DisableVRMovement"); // 2269767341
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_URunebergVR_Movement_Enable360Movement(), "Enable360Movement"); // 3533564718
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_URunebergVR_Movement_EnableVRMovement(), "EnableVRMovement"); // 1712323972
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_URunebergVR_Movement_TimedDashMove(), "TimedDashMove"); // 3804879567
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_URunebergVR_Movement_TimedMovement(), "TimedMovement"); // 686525872
				static TCppClassTypeInfo<TCppClassTypeTraits<URunebergVR_Movement> > StaticCppClassTypeInfo;
				OuterClass->SetCppTypeInfo(&StaticCppClassTypeInfo);
				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("BlueprintSpawnableComponent"), TEXT(""));
				MetaData->SetValue(OuterClass, TEXT("ClassGroupNames"), TEXT("VR"));
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("RunebergVR_Movement.h"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Movement.h"));
				MetaData->SetValue(NewProp_EnableTerrainCheck, TEXT("Category"), TEXT("VR"));
				MetaData->SetValue(NewProp_EnableTerrainCheck, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Movement.h"));
				MetaData->SetValue(NewProp_EnableTerrainCheck, TEXT("ToolTip"), TEXT("Whether this component will check for uneven terrain"));
				MetaData->SetValue(NewProp_IsBouncingBackFromVRBounds, TEXT("Category"), TEXT("VR"));
				MetaData->SetValue(NewProp_IsBouncingBackFromVRBounds, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Movement.h"));
				MetaData->SetValue(NewProp_IsBouncingBackFromVRBounds, TEXT("ToolTip"), TEXT("Indicator if the pawn is bouncing back from a VR bounds collision"));
				MetaData->SetValue(NewProp_IsMoving, TEXT("Category"), TEXT("VR"));
				MetaData->SetValue(NewProp_IsMoving, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Movement.h"));
				MetaData->SetValue(NewProp_IsMoving, TEXT("ToolTip"), TEXT("Indicator if the pawn is moving"));
				MetaData->SetValue(NewProp_NavMeshTolerance, TEXT("Category"), TEXT("VR"));
				MetaData->SetValue(NewProp_NavMeshTolerance, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Movement.h"));
				MetaData->SetValue(NewProp_NavMeshTolerance, TEXT("ToolTip"), TEXT("Navigation mesh tolerance (when used) - fine tune to fit your nav mesh bounds"));
				MetaData->SetValue(NewProp_CurrentMovementDirectionReference, TEXT("Category"), TEXT("VR"));
				MetaData->SetValue(NewProp_CurrentMovementDirectionReference, TEXT("EditInline"), TEXT("true"));
				MetaData->SetValue(NewProp_CurrentMovementDirectionReference, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Movement.h"));
				MetaData->SetValue(NewProp_CurrentMovementDirectionReference, TEXT("ToolTip"), TEXT("Movement Reference - Object that dictates the direction/rotation of the Pawn's movement"));
				MetaData->SetValue(NewProp_OffsetRotation, TEXT("Category"), TEXT("VR"));
				MetaData->SetValue(NewProp_OffsetRotation, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Movement.h"));
				MetaData->SetValue(NewProp_OffsetRotation, TEXT("ToolTip"), TEXT("Offset Rotation (applied per frame)"));
				MetaData->SetValue(NewProp_TargetRotation, TEXT("Category"), TEXT("VR"));
				MetaData->SetValue(NewProp_TargetRotation, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Movement.h"));
				MetaData->SetValue(NewProp_TargetRotation, TEXT("ToolTip"), TEXT("Current Target Rotation (current direction movement component is facing)"));
				MetaData->SetValue(NewProp_CurrentMovementSpeed, TEXT("Category"), TEXT("VR"));
				MetaData->SetValue(NewProp_CurrentMovementSpeed, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_Movement.h"));
				MetaData->SetValue(NewProp_CurrentMovementSpeed, TEXT("ToolTip"), TEXT("Movement Speed"));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	IMPLEMENT_CLASS(URunebergVR_Movement, 4048511900);
	static FCompiledInDefer Z_CompiledInDefer_UClass_URunebergVR_Movement(Z_Construct_UClass_URunebergVR_Movement, &URunebergVR_Movement::StaticClass, TEXT("/Script/RunebergVRPlugin"), TEXT("URunebergVR_Movement"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(URunebergVR_Movement);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
PRAGMA_ENABLE_OPTIMIZATION
