// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "GeneratedCppIncludes.h"
#include "Private/RunebergVRPluginPrivatePCH.h"
#include "Public/RunebergVR_SimpleGrabber.h"
PRAGMA_DISABLE_OPTIMIZATION
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRunebergVR_SimpleGrabber() {}
// Cross Module References
	RUNEBERGVRPLUGIN_API UFunction* Z_Construct_UFunction_URunebergVR_SimpleGrabber_Grab();
	RUNEBERGVRPLUGIN_API UClass* Z_Construct_UClass_URunebergVR_SimpleGrabber();
	RUNEBERGVRPLUGIN_API UFunction* Z_Construct_UFunction_URunebergVR_SimpleGrabber_OnBeginOverlap();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FHitResult();
	ENGINE_API UClass* Z_Construct_UClass_UPrimitiveComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
	RUNEBERGVRPLUGIN_API UFunction* Z_Construct_UFunction_URunebergVR_SimpleGrabber_OnEndOverlap();
	RUNEBERGVRPLUGIN_API UFunction* Z_Construct_UFunction_URunebergVR_SimpleGrabber_Release();
	RUNEBERGVRPLUGIN_API UClass* Z_Construct_UClass_URunebergVR_SimpleGrabber_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_USceneComponent();
	UPackage* Z_Construct_UPackage__Script_RunebergVRPlugin();
// End Cross Module References
	void URunebergVR_SimpleGrabber::StaticRegisterNativesURunebergVR_SimpleGrabber()
	{
		UClass* Class = URunebergVR_SimpleGrabber::StaticClass();
		static const TNameNativePtrPair<ANSICHAR> AnsiFuncs[] = {
			{ "Grab", (Native)&URunebergVR_SimpleGrabber::execGrab },
			{ "OnBeginOverlap", (Native)&URunebergVR_SimpleGrabber::execOnBeginOverlap },
			{ "OnEndOverlap", (Native)&URunebergVR_SimpleGrabber::execOnEndOverlap },
			{ "Release", (Native)&URunebergVR_SimpleGrabber::execRelease },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, AnsiFuncs, ARRAY_COUNT(AnsiFuncs));
	}
	UFunction* Z_Construct_UFunction_URunebergVR_SimpleGrabber_Grab()
	{
		struct RunebergVR_SimpleGrabber_eventGrab_Parms
		{
			int32 _ObjectTypeID;
		};
		UObject* Outer = Z_Construct_UClass_URunebergVR_SimpleGrabber();
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("Grab"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), nullptr, (EFunctionFlags)0x04020401, 65535, sizeof(RunebergVR_SimpleGrabber_eventGrab_Parms));
			UProperty* NewProp__ObjectTypeID = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("_ObjectTypeID"), RF_Public|RF_Transient|RF_MarkAsNative) UUnsizedIntProperty(CPP_PROPERTY_BASE(_ObjectTypeID, RunebergVR_SimpleGrabber_eventGrab_Parms), 0x0010000000000080);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("VR"));
			MetaData->SetValue(ReturnFunction, TEXT("CPP_Default__ObjectTypeID"), TEXT("5"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_SimpleGrabber.h"));
			MetaData->SetValue(ReturnFunction, TEXT("ToolTip"), TEXT("Enable grab"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_URunebergVR_SimpleGrabber_OnBeginOverlap()
	{
		struct RunebergVR_SimpleGrabber_eventOnBeginOverlap_Parms
		{
			UPrimitiveComponent* OverlappedComponent;
			AActor* OtherActor;
			UPrimitiveComponent* OtherComp;
			int32 OtherBodyIndex;
			bool bFromSweep;
			FHitResult SweepResult;
		};
		UObject* Outer = Z_Construct_UClass_URunebergVR_SimpleGrabber();
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("OnBeginOverlap"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), nullptr, (EFunctionFlags)0x00420401, 65535, sizeof(RunebergVR_SimpleGrabber_eventOnBeginOverlap_Parms));
			UProperty* NewProp_SweepResult = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("SweepResult"), RF_Public|RF_Transient|RF_MarkAsNative) UStructProperty(CPP_PROPERTY_BASE(SweepResult, RunebergVR_SimpleGrabber_eventOnBeginOverlap_Parms), 0x0010008008000182, Z_Construct_UScriptStruct_FHitResult());
			CPP_BOOL_PROPERTY_BITMASK_STRUCT(bFromSweep, RunebergVR_SimpleGrabber_eventOnBeginOverlap_Parms);
			UProperty* NewProp_bFromSweep = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("bFromSweep"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(bFromSweep, RunebergVR_SimpleGrabber_eventOnBeginOverlap_Parms), 0x0010000000000080, CPP_BOOL_PROPERTY_BITMASK(bFromSweep, RunebergVR_SimpleGrabber_eventOnBeginOverlap_Parms), sizeof(bool), true);
			UProperty* NewProp_OtherBodyIndex = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("OtherBodyIndex"), RF_Public|RF_Transient|RF_MarkAsNative) UIntProperty(CPP_PROPERTY_BASE(OtherBodyIndex, RunebergVR_SimpleGrabber_eventOnBeginOverlap_Parms), 0x0010000000000080);
			UProperty* NewProp_OtherComp = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("OtherComp"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(OtherComp, RunebergVR_SimpleGrabber_eventOnBeginOverlap_Parms), 0x0010000000080080, Z_Construct_UClass_UPrimitiveComponent_NoRegister());
			UProperty* NewProp_OtherActor = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("OtherActor"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(OtherActor, RunebergVR_SimpleGrabber_eventOnBeginOverlap_Parms), 0x0010000000000080, Z_Construct_UClass_AActor_NoRegister());
			UProperty* NewProp_OverlappedComponent = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("OverlappedComponent"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(OverlappedComponent, RunebergVR_SimpleGrabber_eventOnBeginOverlap_Parms), 0x0010000000080080, Z_Construct_UClass_UPrimitiveComponent_NoRegister());
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_SimpleGrabber.h"));
			MetaData->SetValue(ReturnFunction, TEXT("ToolTip"), TEXT("Delegate function that'll be called during begin overlap"));
			MetaData->SetValue(NewProp_SweepResult, TEXT("NativeConst"), TEXT(""));
			MetaData->SetValue(NewProp_OtherComp, TEXT("EditInline"), TEXT("true"));
			MetaData->SetValue(NewProp_OverlappedComponent, TEXT("EditInline"), TEXT("true"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_URunebergVR_SimpleGrabber_OnEndOverlap()
	{
		struct RunebergVR_SimpleGrabber_eventOnEndOverlap_Parms
		{
			UPrimitiveComponent* OverlappedComponent;
			AActor* OtherActor;
			UPrimitiveComponent* OtherComp;
			int32 OtherBodyIndex;
		};
		UObject* Outer = Z_Construct_UClass_URunebergVR_SimpleGrabber();
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("OnEndOverlap"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), nullptr, (EFunctionFlags)0x00020401, 65535, sizeof(RunebergVR_SimpleGrabber_eventOnEndOverlap_Parms));
			UProperty* NewProp_OtherBodyIndex = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("OtherBodyIndex"), RF_Public|RF_Transient|RF_MarkAsNative) UIntProperty(CPP_PROPERTY_BASE(OtherBodyIndex, RunebergVR_SimpleGrabber_eventOnEndOverlap_Parms), 0x0010000000000080);
			UProperty* NewProp_OtherComp = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("OtherComp"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(OtherComp, RunebergVR_SimpleGrabber_eventOnEndOverlap_Parms), 0x0010000000080080, Z_Construct_UClass_UPrimitiveComponent_NoRegister());
			UProperty* NewProp_OtherActor = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("OtherActor"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(OtherActor, RunebergVR_SimpleGrabber_eventOnEndOverlap_Parms), 0x0010000000000080, Z_Construct_UClass_AActor_NoRegister());
			UProperty* NewProp_OverlappedComponent = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("OverlappedComponent"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(OverlappedComponent, RunebergVR_SimpleGrabber_eventOnEndOverlap_Parms), 0x0010000000080080, Z_Construct_UClass_UPrimitiveComponent_NoRegister());
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_SimpleGrabber.h"));
			MetaData->SetValue(ReturnFunction, TEXT("ToolTip"), TEXT("Delegate function that'll be called during end overlap"));
			MetaData->SetValue(NewProp_OtherComp, TEXT("EditInline"), TEXT("true"));
			MetaData->SetValue(NewProp_OverlappedComponent, TEXT("EditInline"), TEXT("true"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_URunebergVR_SimpleGrabber_Release()
	{
		struct RunebergVR_SimpleGrabber_eventRelease_Parms
		{
			bool EnablePhysics;
		};
		UObject* Outer = Z_Construct_UClass_URunebergVR_SimpleGrabber();
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("Release"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), nullptr, (EFunctionFlags)0x04020401, 65535, sizeof(RunebergVR_SimpleGrabber_eventRelease_Parms));
			CPP_BOOL_PROPERTY_BITMASK_STRUCT(EnablePhysics, RunebergVR_SimpleGrabber_eventRelease_Parms);
			UProperty* NewProp_EnablePhysics = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("EnablePhysics"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(EnablePhysics, RunebergVR_SimpleGrabber_eventRelease_Parms), 0x0010000000000080, CPP_BOOL_PROPERTY_BITMASK(EnablePhysics, RunebergVR_SimpleGrabber_eventRelease_Parms), sizeof(bool), true);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("VR"));
			MetaData->SetValue(ReturnFunction, TEXT("CPP_Default_EnablePhysics"), TEXT("true"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_SimpleGrabber.h"));
			MetaData->SetValue(ReturnFunction, TEXT("ToolTip"), TEXT("Release grabbed object"));
#endif
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_URunebergVR_SimpleGrabber_NoRegister()
	{
		return URunebergVR_SimpleGrabber::StaticClass();
	}
	UClass* Z_Construct_UClass_URunebergVR_SimpleGrabber()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_USceneComponent();
			Z_Construct_UPackage__Script_RunebergVRPlugin();
			OuterClass = URunebergVR_SimpleGrabber::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= (EClassFlags)0x20B00080u;

				OuterClass->LinkChild(Z_Construct_UFunction_URunebergVR_SimpleGrabber_Grab());
				OuterClass->LinkChild(Z_Construct_UFunction_URunebergVR_SimpleGrabber_OnBeginOverlap());
				OuterClass->LinkChild(Z_Construct_UFunction_URunebergVR_SimpleGrabber_OnEndOverlap());
				OuterClass->LinkChild(Z_Construct_UFunction_URunebergVR_SimpleGrabber_Release());

				UProperty* NewProp_GrabSphereRadius = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("GrabSphereRadius"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(GrabSphereRadius, URunebergVR_SimpleGrabber), 0x0010000000000005);
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_URunebergVR_SimpleGrabber_Grab(), "Grab"); // 530739447
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_URunebergVR_SimpleGrabber_OnBeginOverlap(), "OnBeginOverlap"); // 4053139766
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_URunebergVR_SimpleGrabber_OnEndOverlap(), "OnEndOverlap"); // 1574342693
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_URunebergVR_SimpleGrabber_Release(), "Release"); // 774531127
				static TCppClassTypeInfo<TCppClassTypeTraits<URunebergVR_SimpleGrabber> > StaticCppClassTypeInfo;
				OuterClass->SetCppTypeInfo(&StaticCppClassTypeInfo);
				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("BlueprintSpawnableComponent"), TEXT(""));
				MetaData->SetValue(OuterClass, TEXT("ClassGroupNames"), TEXT("VR"));
				MetaData->SetValue(OuterClass, TEXT("HideCategories"), TEXT("Trigger PhysicsVolume"));
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("RunebergVR_SimpleGrabber.h"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_SimpleGrabber.h"));
				MetaData->SetValue(NewProp_GrabSphereRadius, TEXT("Category"), TEXT("VR"));
				MetaData->SetValue(NewProp_GrabSphereRadius, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_SimpleGrabber.h"));
				MetaData->SetValue(NewProp_GrabSphereRadius, TEXT("ToolTip"), TEXT("Object Type that'll be \"grabbable\" (PhysicsBody)"));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	IMPLEMENT_CLASS(URunebergVR_SimpleGrabber, 4282368561);
	static FCompiledInDefer Z_CompiledInDefer_UClass_URunebergVR_SimpleGrabber(Z_Construct_UClass_URunebergVR_SimpleGrabber, &URunebergVR_SimpleGrabber::StaticClass, TEXT("/Script/RunebergVRPlugin"), TEXT("URunebergVR_SimpleGrabber"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(URunebergVR_SimpleGrabber);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
PRAGMA_ENABLE_OPTIMIZATION
