// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "GeneratedCppIncludes.h"
#include "Private/RunebergVRPluginPrivatePCH.h"
#include "Public/RunebergVR_ScalableMesh.h"
PRAGMA_DISABLE_OPTIMIZATION
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRunebergVR_ScalableMesh() {}
// Cross Module References
	RUNEBERGVRPLUGIN_API UEnum* Z_Construct_UEnum_RunebergVRPlugin_EScaleDirectionEnum();
	UPackage* Z_Construct_UPackage__Script_RunebergVRPlugin();
	RUNEBERGVRPLUGIN_API UEnum* Z_Construct_UEnum_RunebergVRPlugin_EScaleModeEnum();
	RUNEBERGVRPLUGIN_API UFunction* Z_Construct_UFunction_URunebergVR_ScalableMesh_ScaleDownAndMove();
	RUNEBERGVRPLUGIN_API UClass* Z_Construct_UClass_URunebergVR_ScalableMesh();
	RUNEBERGVRPLUGIN_API UFunction* Z_Construct_UFunction_URunebergVR_ScalableMesh_ScaleMeshDown();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	RUNEBERGVRPLUGIN_API UFunction* Z_Construct_UFunction_URunebergVR_ScalableMesh_ScaleMeshToLocation();
	RUNEBERGVRPLUGIN_API UFunction* Z_Construct_UFunction_URunebergVR_ScalableMesh_ScaleMeshUp();
	RUNEBERGVRPLUGIN_API UClass* Z_Construct_UClass_URunebergVR_ScalableMesh_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_USceneComponent();
	ENGINE_API UClass* Z_Construct_UClass_USceneComponent_NoRegister();
// End Cross Module References
static UEnum* EScaleDirectionEnum_StaticEnum()
{
	static UEnum* Singleton = nullptr;
	if (!Singleton)
	{
		Singleton = GetStaticEnum(Z_Construct_UEnum_RunebergVRPlugin_EScaleDirectionEnum, Z_Construct_UPackage__Script_RunebergVRPlugin(), TEXT("EScaleDirectionEnum"));
	}
	return Singleton;
}
static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EScaleDirectionEnum(EScaleDirectionEnum_StaticEnum, TEXT("/Script/RunebergVRPlugin"), TEXT("EScaleDirectionEnum"), false, nullptr, nullptr);
	UEnum* Z_Construct_UEnum_RunebergVRPlugin_EScaleDirectionEnum()
	{
		UPackage* Outer = Z_Construct_UPackage__Script_RunebergVRPlugin();
		extern uint32 Get_Z_Construct_UEnum_RunebergVRPlugin_EScaleDirectionEnum_CRC();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EScaleDirectionEnum"), 0, Get_Z_Construct_UEnum_RunebergVRPlugin_EScaleDirectionEnum_CRC(), false);
		if (!ReturnEnum)
		{
			ReturnEnum = new(EC_InternalUseOnlyConstructor, Outer, TEXT("EScaleDirectionEnum"), RF_Public|RF_Transient|RF_MarkAsNative) UEnum(FObjectInitializer());
			TArray<TPair<FName, int64>> EnumNames;
			EnumNames.Emplace(TEXT("EScaleDirectionEnum::SCALE_X"), 0);
			EnumNames.Emplace(TEXT("EScaleDirectionEnum::SCALE_Y"), 1);
			EnumNames.Emplace(TEXT("EScaleDirectionEnum::SCALE_Z"), 2);
			EnumNames.Emplace(TEXT("EScaleDirectionEnum::SCALE_MAX"), 3);
			ReturnEnum->SetEnums(EnumNames, UEnum::ECppForm::EnumClass);
			ReturnEnum->CppType = TEXT("EScaleDirectionEnum");
#if WITH_METADATA
			UMetaData* MetaData = ReturnEnum->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnEnum, TEXT("BlueprintType"), TEXT("true"));
			MetaData->SetValue(ReturnEnum, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_ScalableMesh.h"));
			MetaData->SetValue(ReturnEnum, TEXT("SCALE_X.DisplayName"), TEXT("Scale mesh in X"));
			MetaData->SetValue(ReturnEnum, TEXT("SCALE_Y.DisplayName"), TEXT("Scale mesh in Y"));
			MetaData->SetValue(ReturnEnum, TEXT("SCALE_Z.DisplayName"), TEXT("Scale mesh in Z"));
#endif
		}
		return ReturnEnum;
	}
	uint32 Get_Z_Construct_UEnum_RunebergVRPlugin_EScaleDirectionEnum_CRC() { return 3355742639U; }
static UEnum* EScaleModeEnum_StaticEnum()
{
	static UEnum* Singleton = nullptr;
	if (!Singleton)
	{
		Singleton = GetStaticEnum(Z_Construct_UEnum_RunebergVRPlugin_EScaleModeEnum, Z_Construct_UPackage__Script_RunebergVRPlugin(), TEXT("EScaleModeEnum"));
	}
	return Singleton;
}
static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EScaleModeEnum(EScaleModeEnum_StaticEnum, TEXT("/Script/RunebergVRPlugin"), TEXT("EScaleModeEnum"), false, nullptr, nullptr);
	UEnum* Z_Construct_UEnum_RunebergVRPlugin_EScaleModeEnum()
	{
		UPackage* Outer = Z_Construct_UPackage__Script_RunebergVRPlugin();
		extern uint32 Get_Z_Construct_UEnum_RunebergVRPlugin_EScaleModeEnum_CRC();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EScaleModeEnum"), 0, Get_Z_Construct_UEnum_RunebergVRPlugin_EScaleModeEnum_CRC(), false);
		if (!ReturnEnum)
		{
			ReturnEnum = new(EC_InternalUseOnlyConstructor, Outer, TEXT("EScaleModeEnum"), RF_Public|RF_Transient|RF_MarkAsNative) UEnum(FObjectInitializer());
			TArray<TPair<FName, int64>> EnumNames;
			EnumNames.Emplace(TEXT("EScaleModeEnum::SCALE_TO_MAX"), 0);
			EnumNames.Emplace(TEXT("EScaleModeEnum::SCALE_TO_MIN"), 1);
			EnumNames.Emplace(TEXT("EScaleModeEnum::SCALE_TO_LOC"), 2);
			EnumNames.Emplace(TEXT("EScaleModeEnum::SCALE_DN_MOV"), 3);
			EnumNames.Emplace(TEXT("EScaleModeEnum::SCALE_MAX"), 4);
			ReturnEnum->SetEnums(EnumNames, UEnum::ECppForm::EnumClass);
			ReturnEnum->CppType = TEXT("EScaleModeEnum");
#if WITH_METADATA
			UMetaData* MetaData = ReturnEnum->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnEnum, TEXT("BlueprintType"), TEXT("true"));
			MetaData->SetValue(ReturnEnum, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_ScalableMesh.h"));
			MetaData->SetValue(ReturnEnum, TEXT("SCALE_DN_MOV.DisplayName"), TEXT("Scale down while getting player to location"));
			MetaData->SetValue(ReturnEnum, TEXT("SCALE_TO_LOC.DisplayName"), TEXT("Scale up to defined location"));
			MetaData->SetValue(ReturnEnum, TEXT("SCALE_TO_MAX.DisplayName"), TEXT("Scale up to max distance"));
			MetaData->SetValue(ReturnEnum, TEXT("SCALE_TO_MIN.DisplayName"), TEXT("Scale down to min distance"));
#endif
		}
		return ReturnEnum;
	}
	uint32 Get_Z_Construct_UEnum_RunebergVRPlugin_EScaleModeEnum_CRC() { return 4265161152U; }
	void URunebergVR_ScalableMesh::StaticRegisterNativesURunebergVR_ScalableMesh()
	{
		UClass* Class = URunebergVR_ScalableMesh::StaticClass();
		static const TNameNativePtrPair<ANSICHAR> AnsiFuncs[] = {
			{ "ScaleDownAndMove", (Native)&URunebergVR_ScalableMesh::execScaleDownAndMove },
			{ "ScaleMeshDown", (Native)&URunebergVR_ScalableMesh::execScaleMeshDown },
			{ "ScaleMeshToLocation", (Native)&URunebergVR_ScalableMesh::execScaleMeshToLocation },
			{ "ScaleMeshUp", (Native)&URunebergVR_ScalableMesh::execScaleMeshUp },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, AnsiFuncs, ARRAY_COUNT(AnsiFuncs));
	}
	UFunction* Z_Construct_UFunction_URunebergVR_ScalableMesh_ScaleDownAndMove()
	{
		struct RunebergVR_ScalableMesh_eventScaleDownAndMove_Parms
		{
			EScaleDirectionEnum Scale_Direction;
			float Rate;
			float DistanceCorrection;
			bool VisibilityAfterScale;
		};
		UObject* Outer = Z_Construct_UClass_URunebergVR_ScalableMesh();
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("ScaleDownAndMove"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), nullptr, (EFunctionFlags)0x04020401, 65535, sizeof(RunebergVR_ScalableMesh_eventScaleDownAndMove_Parms));
			CPP_BOOL_PROPERTY_BITMASK_STRUCT(VisibilityAfterScale, RunebergVR_ScalableMesh_eventScaleDownAndMove_Parms);
			UProperty* NewProp_VisibilityAfterScale = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("VisibilityAfterScale"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(VisibilityAfterScale, RunebergVR_ScalableMesh_eventScaleDownAndMove_Parms), 0x0010000000000080, CPP_BOOL_PROPERTY_BITMASK(VisibilityAfterScale, RunebergVR_ScalableMesh_eventScaleDownAndMove_Parms), sizeof(bool), true);
			UProperty* NewProp_DistanceCorrection = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("DistanceCorrection"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(DistanceCorrection, RunebergVR_ScalableMesh_eventScaleDownAndMove_Parms), 0x0010000000000080);
			UProperty* NewProp_Rate = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("Rate"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(Rate, RunebergVR_ScalableMesh_eventScaleDownAndMove_Parms), 0x0010000000000080);
			UProperty* NewProp_Scale_Direction = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("Scale_Direction"), RF_Public|RF_Transient|RF_MarkAsNative) UEnumProperty(CPP_PROPERTY_BASE(Scale_Direction, RunebergVR_ScalableMesh_eventScaleDownAndMove_Parms), 0x0010000000000080, Z_Construct_UEnum_RunebergVRPlugin_EScaleDirectionEnum());
			UProperty* NewProp_Scale_Direction_Underlying = new(EC_InternalUseOnlyConstructor, NewProp_Scale_Direction, TEXT("UnderlyingType"), RF_Public|RF_Transient|RF_MarkAsNative) UByteProperty(FObjectInitializer(), EC_CppProperty, 0, 0x0000000000000000);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("VR"));
			MetaData->SetValue(ReturnFunction, TEXT("CPP_Default_DistanceCorrection"), TEXT("100.000000"));
			MetaData->SetValue(ReturnFunction, TEXT("CPP_Default_Rate"), TEXT("1.000000"));
			MetaData->SetValue(ReturnFunction, TEXT("CPP_Default_Scale_Direction"), TEXT("SCALE_X"));
			MetaData->SetValue(ReturnFunction, TEXT("CPP_Default_VisibilityAfterScale"), TEXT("false"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_ScalableMesh.h"));
			MetaData->SetValue(ReturnFunction, TEXT("ToolTip"), TEXT("Scale mesh down and move pawn to target location"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_URunebergVR_ScalableMesh_ScaleMeshDown()
	{
		struct RunebergVR_ScalableMesh_eventScaleMeshDown_Parms
		{
			FVector DistanceToScaleDownXYZ;
			FVector RateXYZ;
			bool VisibilityAfterScale;
		};
		UObject* Outer = Z_Construct_UClass_URunebergVR_ScalableMesh();
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("ScaleMeshDown"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), nullptr, (EFunctionFlags)0x04820401, 65535, sizeof(RunebergVR_ScalableMesh_eventScaleMeshDown_Parms));
			CPP_BOOL_PROPERTY_BITMASK_STRUCT(VisibilityAfterScale, RunebergVR_ScalableMesh_eventScaleMeshDown_Parms);
			UProperty* NewProp_VisibilityAfterScale = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("VisibilityAfterScale"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(VisibilityAfterScale, RunebergVR_ScalableMesh_eventScaleMeshDown_Parms), 0x0010000000000080, CPP_BOOL_PROPERTY_BITMASK(VisibilityAfterScale, RunebergVR_ScalableMesh_eventScaleMeshDown_Parms), sizeof(bool), true);
			UProperty* NewProp_RateXYZ = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("RateXYZ"), RF_Public|RF_Transient|RF_MarkAsNative) UStructProperty(CPP_PROPERTY_BASE(RateXYZ, RunebergVR_ScalableMesh_eventScaleMeshDown_Parms), 0x0010000000000080, Z_Construct_UScriptStruct_FVector());
			UProperty* NewProp_DistanceToScaleDownXYZ = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("DistanceToScaleDownXYZ"), RF_Public|RF_Transient|RF_MarkAsNative) UStructProperty(CPP_PROPERTY_BASE(DistanceToScaleDownXYZ, RunebergVR_ScalableMesh_eventScaleMeshDown_Parms), 0x0010000000000080, Z_Construct_UScriptStruct_FVector());
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("VR"));
			MetaData->SetValue(ReturnFunction, TEXT("CPP_Default_DistanceToScaleDownXYZ"), TEXT("112.000000,0.000000,0.000000"));
			MetaData->SetValue(ReturnFunction, TEXT("CPP_Default_RateXYZ"), TEXT("1.500000,0.000000,0.000000"));
			MetaData->SetValue(ReturnFunction, TEXT("CPP_Default_VisibilityAfterScale"), TEXT("false"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_ScalableMesh.h"));
			MetaData->SetValue(ReturnFunction, TEXT("ToolTip"), TEXT("Scale mesh to specified distance amount"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_URunebergVR_ScalableMesh_ScaleMeshToLocation()
	{
		struct RunebergVR_ScalableMesh_eventScaleMeshToLocation_Parms
		{
			FVector Target_Location;
			EScaleDirectionEnum Scale_Direction;
			float Rate;
			bool NewVisibility;
		};
		UObject* Outer = Z_Construct_UClass_URunebergVR_ScalableMesh();
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("ScaleMeshToLocation"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), nullptr, (EFunctionFlags)0x04820401, 65535, sizeof(RunebergVR_ScalableMesh_eventScaleMeshToLocation_Parms));
			CPP_BOOL_PROPERTY_BITMASK_STRUCT(NewVisibility, RunebergVR_ScalableMesh_eventScaleMeshToLocation_Parms);
			UProperty* NewProp_NewVisibility = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("NewVisibility"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(NewVisibility, RunebergVR_ScalableMesh_eventScaleMeshToLocation_Parms), 0x0010000000000080, CPP_BOOL_PROPERTY_BITMASK(NewVisibility, RunebergVR_ScalableMesh_eventScaleMeshToLocation_Parms), sizeof(bool), true);
			UProperty* NewProp_Rate = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("Rate"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(Rate, RunebergVR_ScalableMesh_eventScaleMeshToLocation_Parms), 0x0010000000000080);
			UProperty* NewProp_Scale_Direction = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("Scale_Direction"), RF_Public|RF_Transient|RF_MarkAsNative) UEnumProperty(CPP_PROPERTY_BASE(Scale_Direction, RunebergVR_ScalableMesh_eventScaleMeshToLocation_Parms), 0x0010000000000080, Z_Construct_UEnum_RunebergVRPlugin_EScaleDirectionEnum());
			UProperty* NewProp_Scale_Direction_Underlying = new(EC_InternalUseOnlyConstructor, NewProp_Scale_Direction, TEXT("UnderlyingType"), RF_Public|RF_Transient|RF_MarkAsNative) UByteProperty(FObjectInitializer(), EC_CppProperty, 0, 0x0000000000000000);
			UProperty* NewProp_Target_Location = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("Target_Location"), RF_Public|RF_Transient|RF_MarkAsNative) UStructProperty(CPP_PROPERTY_BASE(Target_Location, RunebergVR_ScalableMesh_eventScaleMeshToLocation_Parms), 0x0010000000000080, Z_Construct_UScriptStruct_FVector());
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("VR"));
			MetaData->SetValue(ReturnFunction, TEXT("CPP_Default_NewVisibility"), TEXT("true"));
			MetaData->SetValue(ReturnFunction, TEXT("CPP_Default_Rate"), TEXT("1.000000"));
			MetaData->SetValue(ReturnFunction, TEXT("CPP_Default_Scale_Direction"), TEXT("SCALE_X"));
			MetaData->SetValue(ReturnFunction, TEXT("CPP_Default_Target_Location"), TEXT("0.000000,0.000000,0.000000"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_ScalableMesh.h"));
			MetaData->SetValue(ReturnFunction, TEXT("ToolTip"), TEXT("Scale mesh to specified distance amount"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_URunebergVR_ScalableMesh_ScaleMeshUp()
	{
		struct RunebergVR_ScalableMesh_eventScaleMeshUp_Parms
		{
			FVector DistanceToScaleUpXYZ;
			FVector RateXYZ;
			bool NewVisibility;
		};
		UObject* Outer = Z_Construct_UClass_URunebergVR_ScalableMesh();
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("ScaleMeshUp"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), nullptr, (EFunctionFlags)0x04820401, 65535, sizeof(RunebergVR_ScalableMesh_eventScaleMeshUp_Parms));
			CPP_BOOL_PROPERTY_BITMASK_STRUCT(NewVisibility, RunebergVR_ScalableMesh_eventScaleMeshUp_Parms);
			UProperty* NewProp_NewVisibility = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("NewVisibility"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(NewVisibility, RunebergVR_ScalableMesh_eventScaleMeshUp_Parms), 0x0010000000000080, CPP_BOOL_PROPERTY_BITMASK(NewVisibility, RunebergVR_ScalableMesh_eventScaleMeshUp_Parms), sizeof(bool), true);
			UProperty* NewProp_RateXYZ = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("RateXYZ"), RF_Public|RF_Transient|RF_MarkAsNative) UStructProperty(CPP_PROPERTY_BASE(RateXYZ, RunebergVR_ScalableMesh_eventScaleMeshUp_Parms), 0x0010000000000080, Z_Construct_UScriptStruct_FVector());
			UProperty* NewProp_DistanceToScaleUpXYZ = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("DistanceToScaleUpXYZ"), RF_Public|RF_Transient|RF_MarkAsNative) UStructProperty(CPP_PROPERTY_BASE(DistanceToScaleUpXYZ, RunebergVR_ScalableMesh_eventScaleMeshUp_Parms), 0x0010000000000080, Z_Construct_UScriptStruct_FVector());
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("VR"));
			MetaData->SetValue(ReturnFunction, TEXT("CPP_Default_DistanceToScaleUpXYZ"), TEXT("112.000000,0.000000,0.000000"));
			MetaData->SetValue(ReturnFunction, TEXT("CPP_Default_NewVisibility"), TEXT("true"));
			MetaData->SetValue(ReturnFunction, TEXT("CPP_Default_RateXYZ"), TEXT("1.500000,0.000000,0.000000"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_ScalableMesh.h"));
			MetaData->SetValue(ReturnFunction, TEXT("ToolTip"), TEXT("Scale mesh to specified distance amount"));
#endif
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_URunebergVR_ScalableMesh_NoRegister()
	{
		return URunebergVR_ScalableMesh::StaticClass();
	}
	UClass* Z_Construct_UClass_URunebergVR_ScalableMesh()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_USceneComponent();
			Z_Construct_UPackage__Script_RunebergVRPlugin();
			OuterClass = URunebergVR_ScalableMesh::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= (EClassFlags)0x20B00080u;

				OuterClass->LinkChild(Z_Construct_UFunction_URunebergVR_ScalableMesh_ScaleDownAndMove());
				OuterClass->LinkChild(Z_Construct_UFunction_URunebergVR_ScalableMesh_ScaleMeshDown());
				OuterClass->LinkChild(Z_Construct_UFunction_URunebergVR_ScalableMesh_ScaleMeshToLocation());
				OuterClass->LinkChild(Z_Construct_UFunction_URunebergVR_ScalableMesh_ScaleMeshUp());

				CPP_BOOL_PROPERTY_BITMASK_STRUCT(IsScaling, URunebergVR_ScalableMesh);
				UProperty* NewProp_IsScaling = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("IsScaling"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(IsScaling, URunebergVR_ScalableMesh), 0x0010000000000005, CPP_BOOL_PROPERTY_BITMASK(IsScaling, URunebergVR_ScalableMesh), sizeof(bool), true);
				UProperty* NewProp_MeshChildren = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("MeshChildren"), RF_Public|RF_Transient|RF_MarkAsNative) UArrayProperty(CPP_PROPERTY_BASE(MeshChildren, URunebergVR_ScalableMesh), 0x001000800000000d);
				UProperty* NewProp_MeshChildren_Inner = new(EC_InternalUseOnlyConstructor, NewProp_MeshChildren, TEXT("MeshChildren"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(FObjectInitializer(), EC_CppProperty, 0, 0x0000000000080008, Z_Construct_UClass_USceneComponent_NoRegister());
				UProperty* NewProp_TargetLocation = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("TargetLocation"), RF_Public|RF_Transient|RF_MarkAsNative) UStructProperty(CPP_PROPERTY_BASE(TargetLocation, URunebergVR_ScalableMesh), 0x0010000000000005, Z_Construct_UScriptStruct_FVector());
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_URunebergVR_ScalableMesh_ScaleDownAndMove(), "ScaleDownAndMove"); // 1531687152
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_URunebergVR_ScalableMesh_ScaleMeshDown(), "ScaleMeshDown"); // 1489240006
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_URunebergVR_ScalableMesh_ScaleMeshToLocation(), "ScaleMeshToLocation"); // 1442169149
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_URunebergVR_ScalableMesh_ScaleMeshUp(), "ScaleMeshUp"); // 35925280
				static TCppClassTypeInfo<TCppClassTypeTraits<URunebergVR_ScalableMesh> > StaticCppClassTypeInfo;
				OuterClass->SetCppTypeInfo(&StaticCppClassTypeInfo);
				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("BlueprintSpawnableComponent"), TEXT(""));
				MetaData->SetValue(OuterClass, TEXT("ClassGroupNames"), TEXT("VR"));
				MetaData->SetValue(OuterClass, TEXT("HideCategories"), TEXT("Trigger PhysicsVolume"));
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("RunebergVR_ScalableMesh.h"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_ScalableMesh.h"));
				MetaData->SetValue(NewProp_IsScaling, TEXT("Category"), TEXT("VR"));
				MetaData->SetValue(NewProp_IsScaling, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_ScalableMesh.h"));
				MetaData->SetValue(NewProp_IsScaling, TEXT("ToolTip"), TEXT("Check if currently scaling a mesh"));
				MetaData->SetValue(NewProp_MeshChildren, TEXT("Category"), TEXT("VR"));
				MetaData->SetValue(NewProp_MeshChildren, TEXT("EditInline"), TEXT("true"));
				MetaData->SetValue(NewProp_MeshChildren, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_ScalableMesh.h"));
				MetaData->SetValue(NewProp_MeshChildren, TEXT("ToolTip"), TEXT("Children Components -  Static Mesh and Skeletal Mesh components only, updated during runtume"));
				MetaData->SetValue(NewProp_TargetLocation, TEXT("Category"), TEXT("VR"));
				MetaData->SetValue(NewProp_TargetLocation, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_ScalableMesh.h"));
				MetaData->SetValue(NewProp_TargetLocation, TEXT("ToolTip"), TEXT("Target Location"));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	IMPLEMENT_CLASS(URunebergVR_ScalableMesh, 3160686557);
	static FCompiledInDefer Z_CompiledInDefer_UClass_URunebergVR_ScalableMesh(Z_Construct_UClass_URunebergVR_ScalableMesh, &URunebergVR_ScalableMesh::StaticClass, TEXT("/Script/RunebergVRPlugin"), TEXT("URunebergVR_ScalableMesh"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(URunebergVR_ScalableMesh);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
PRAGMA_ENABLE_OPTIMIZATION
