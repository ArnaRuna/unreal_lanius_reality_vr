// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "GeneratedCppIncludes.h"
#include "Private/RunebergVRPluginPrivatePCH.h"
#include "Public/RunebergVR_CustomGravity.h"
PRAGMA_DISABLE_OPTIMIZATION
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRunebergVR_CustomGravity() {}
// Cross Module References
	RUNEBERGVRPLUGIN_API UEnum* Z_Construct_UEnum_RunebergVRPlugin_EGravityDirection();
	UPackage* Z_Construct_UPackage__Script_RunebergVRPlugin();
	RUNEBERGVRPLUGIN_API UFunction* Z_Construct_UFunction_URunebergVR_CustomGravity_ProcessTags();
	RUNEBERGVRPLUGIN_API UClass* Z_Construct_UClass_URunebergVR_CustomGravity();
	ENGINE_API UClass* Z_Construct_UClass_UPrimitiveComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
	RUNEBERGVRPLUGIN_API UClass* Z_Construct_UClass_URunebergVR_CustomGravity_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UActorComponent();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
// End Cross Module References
static UEnum* EGravityDirection_StaticEnum()
{
	static UEnum* Singleton = nullptr;
	if (!Singleton)
	{
		Singleton = GetStaticEnum(Z_Construct_UEnum_RunebergVRPlugin_EGravityDirection, Z_Construct_UPackage__Script_RunebergVRPlugin(), TEXT("EGravityDirection"));
	}
	return Singleton;
}
static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EGravityDirection(EGravityDirection_StaticEnum, TEXT("/Script/RunebergVRPlugin"), TEXT("EGravityDirection"), false, nullptr, nullptr);
	UEnum* Z_Construct_UEnum_RunebergVRPlugin_EGravityDirection()
	{
		UPackage* Outer = Z_Construct_UPackage__Script_RunebergVRPlugin();
		extern uint32 Get_Z_Construct_UEnum_RunebergVRPlugin_EGravityDirection_CRC();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EGravityDirection"), 0, Get_Z_Construct_UEnum_RunebergVRPlugin_EGravityDirection_CRC(), false);
		if (!ReturnEnum)
		{
			ReturnEnum = new(EC_InternalUseOnlyConstructor, Outer, TEXT("EGravityDirection"), RF_Public|RF_Transient|RF_MarkAsNative) UEnum(FObjectInitializer());
			TArray<TPair<FName, int64>> EnumNames;
			EnumNames.Emplace(TEXT("EGravityDirection::DIR_DOWN"), 0);
			EnumNames.Emplace(TEXT("EGravityDirection::DIR_UP"), 1);
			EnumNames.Emplace(TEXT("EGravityDirection::DIR_LEFT"), 2);
			EnumNames.Emplace(TEXT("EGravityDirection::DIR_RIGHT"), 3);
			EnumNames.Emplace(TEXT("EGravityDirection::DIR_FORWARD"), 4);
			EnumNames.Emplace(TEXT("EGravityDirection::DIR_BACK"), 5);
			EnumNames.Emplace(TEXT("EGravityDirection::DIR_RELATIVE"), 6);
			EnumNames.Emplace(TEXT("EGravityDirection::DIR_MAX"), 7);
			ReturnEnum->SetEnums(EnumNames, UEnum::ECppForm::EnumClass);
			ReturnEnum->CppType = TEXT("EGravityDirection");
#if WITH_METADATA
			UMetaData* MetaData = ReturnEnum->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnEnum, TEXT("BlueprintType"), TEXT("true"));
			MetaData->SetValue(ReturnEnum, TEXT("DIR_BACK.DisplayName"), TEXT("Back"));
			MetaData->SetValue(ReturnEnum, TEXT("DIR_DOWN.DisplayName"), TEXT("Down"));
			MetaData->SetValue(ReturnEnum, TEXT("DIR_FORWARD.DisplayName"), TEXT("Forward"));
			MetaData->SetValue(ReturnEnum, TEXT("DIR_LEFT.DisplayName"), TEXT("Left"));
			MetaData->SetValue(ReturnEnum, TEXT("DIR_RELATIVE.DisplayName"), TEXT("Relative"));
			MetaData->SetValue(ReturnEnum, TEXT("DIR_RIGHT.DisplayName"), TEXT("Right"));
			MetaData->SetValue(ReturnEnum, TEXT("DIR_UP.DisplayName"), TEXT("Up"));
			MetaData->SetValue(ReturnEnum, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_CustomGravity.h"));
#endif
		}
		return ReturnEnum;
	}
	uint32 Get_Z_Construct_UEnum_RunebergVRPlugin_EGravityDirection_CRC() { return 365791260U; }
	void URunebergVR_CustomGravity::StaticRegisterNativesURunebergVR_CustomGravity()
	{
		UClass* Class = URunebergVR_CustomGravity::StaticClass();
		static const TNameNativePtrPair<ANSICHAR> AnsiFuncs[] = {
			{ "ProcessTags", (Native)&URunebergVR_CustomGravity::execProcessTags },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, AnsiFuncs, ARRAY_COUNT(AnsiFuncs));
	}
	UFunction* Z_Construct_UFunction_URunebergVR_CustomGravity_ProcessTags()
	{
		struct RunebergVR_CustomGravity_eventProcessTags_Parms
		{
			AActor* OtherActor;
			UPrimitiveComponent* OtherComp;
		};
		UObject* Outer = Z_Construct_UClass_URunebergVR_CustomGravity();
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("ProcessTags"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), nullptr, (EFunctionFlags)0x04020401, 65535, sizeof(RunebergVR_CustomGravity_eventProcessTags_Parms));
			UProperty* NewProp_OtherComp = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("OtherComp"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(OtherComp, RunebergVR_CustomGravity_eventProcessTags_Parms), 0x0010000000080080, Z_Construct_UClass_UPrimitiveComponent_NoRegister());
			UProperty* NewProp_OtherActor = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("OtherActor"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(OtherActor, RunebergVR_CustomGravity_eventProcessTags_Parms), 0x0010000000000080, Z_Construct_UClass_AActor_NoRegister());
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("VR"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_CustomGravity.h"));
			MetaData->SetValue(ReturnFunction, TEXT("ToolTip"), TEXT("Stop or Start Gravity if collided component or actor has the corresponding start/stop tag"));
			MetaData->SetValue(NewProp_OtherComp, TEXT("EditInline"), TEXT("true"));
#endif
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_URunebergVR_CustomGravity_NoRegister()
	{
		return URunebergVR_CustomGravity::StaticClass();
	}
	UClass* Z_Construct_UClass_URunebergVR_CustomGravity()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_UActorComponent();
			Z_Construct_UPackage__Script_RunebergVRPlugin();
			OuterClass = URunebergVR_CustomGravity::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= (EClassFlags)0x20B00080u;

				OuterClass->LinkChild(Z_Construct_UFunction_URunebergVR_CustomGravity_ProcessTags());

				CPP_BOOL_PROPERTY_BITMASK_STRUCT(IsGravityActive, URunebergVR_CustomGravity);
				UProperty* NewProp_IsGravityActive = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("IsGravityActive"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(IsGravityActive, URunebergVR_CustomGravity), 0x0010000000000005, CPP_BOOL_PROPERTY_BITMASK(IsGravityActive, URunebergVR_CustomGravity), sizeof(bool), true);
				UProperty* NewProp_StartTags = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("StartTags"), RF_Public|RF_Transient|RF_MarkAsNative) UArrayProperty(CPP_PROPERTY_BASE(StartTags, URunebergVR_CustomGravity), 0x0010000000000005);
				UProperty* NewProp_StartTags_Inner = new(EC_InternalUseOnlyConstructor, NewProp_StartTags, TEXT("StartTags"), RF_Public|RF_Transient|RF_MarkAsNative) UNameProperty(FObjectInitializer(), EC_CppProperty, 0, 0x0000000000000000);
				UProperty* NewProp_StopTags = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("StopTags"), RF_Public|RF_Transient|RF_MarkAsNative) UArrayProperty(CPP_PROPERTY_BASE(StopTags, URunebergVR_CustomGravity), 0x0010000000000005);
				UProperty* NewProp_StopTags_Inner = new(EC_InternalUseOnlyConstructor, NewProp_StopTags, TEXT("StopTags"), RF_Public|RF_Transient|RF_MarkAsNative) UNameProperty(FObjectInitializer(), EC_CppProperty, 0, 0x0000000000000000);
				UProperty* NewProp_GravityOrigin = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("GravityOrigin"), RF_Public|RF_Transient|RF_MarkAsNative) UStructProperty(CPP_PROPERTY_BASE(GravityOrigin, URunebergVR_CustomGravity), 0x0010000000000005, Z_Construct_UScriptStruct_FVector());
				UProperty* NewProp_GravityStrength = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("GravityStrength"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(GravityStrength, URunebergVR_CustomGravity), 0x0010000000000005);
				UProperty* NewProp_GravityDirection = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("GravityDirection"), RF_Public|RF_Transient|RF_MarkAsNative) UEnumProperty(CPP_PROPERTY_BASE(GravityDirection, URunebergVR_CustomGravity), 0x0010000000000005, Z_Construct_UEnum_RunebergVRPlugin_EGravityDirection());
				UProperty* NewProp_GravityDirection_Underlying = new(EC_InternalUseOnlyConstructor, NewProp_GravityDirection, TEXT("UnderlyingType"), RF_Public|RF_Transient|RF_MarkAsNative) UByteProperty(FObjectInitializer(), EC_CppProperty, 0, 0x0000000000000000);
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_URunebergVR_CustomGravity_ProcessTags(), "ProcessTags"); // 2419133302
				static TCppClassTypeInfo<TCppClassTypeTraits<URunebergVR_CustomGravity> > StaticCppClassTypeInfo;
				OuterClass->SetCppTypeInfo(&StaticCppClassTypeInfo);
				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("BlueprintSpawnableComponent"), TEXT(""));
				MetaData->SetValue(OuterClass, TEXT("ClassGroupNames"), TEXT("VR"));
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("RunebergVR_CustomGravity.h"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_CustomGravity.h"));
				MetaData->SetValue(NewProp_IsGravityActive, TEXT("Category"), TEXT("VR"));
				MetaData->SetValue(NewProp_IsGravityActive, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_CustomGravity.h"));
				MetaData->SetValue(NewProp_IsGravityActive, TEXT("ToolTip"), TEXT("Whether this actor is currently falling"));
				MetaData->SetValue(NewProp_StartTags, TEXT("Category"), TEXT("VR"));
				MetaData->SetValue(NewProp_StartTags, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_CustomGravity.h"));
				MetaData->SetValue(NewProp_StartTags, TEXT("ToolTip"), TEXT("Tags that can be used to start gravity"));
				MetaData->SetValue(NewProp_StopTags, TEXT("Category"), TEXT("VR"));
				MetaData->SetValue(NewProp_StopTags, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_CustomGravity.h"));
				MetaData->SetValue(NewProp_StopTags, TEXT("ToolTip"), TEXT("Tags that can be used to stop gravity"));
				MetaData->SetValue(NewProp_GravityOrigin, TEXT("Category"), TEXT("VR"));
				MetaData->SetValue(NewProp_GravityOrigin, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_CustomGravity.h"));
				MetaData->SetValue(NewProp_GravityOrigin, TEXT("ToolTip"), TEXT("Gravity origin when gravity direction is set to RELATIVE"));
				MetaData->SetValue(NewProp_GravityStrength, TEXT("Category"), TEXT("VR"));
				MetaData->SetValue(NewProp_GravityStrength, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_CustomGravity.h"));
				MetaData->SetValue(NewProp_GravityStrength, TEXT("ToolTip"), TEXT("Rate of gravity's acceleration"));
				MetaData->SetValue(NewProp_GravityDirection, TEXT("Category"), TEXT("VR"));
				MetaData->SetValue(NewProp_GravityDirection, TEXT("ModuleRelativePath"), TEXT("Public/RunebergVR_CustomGravity.h"));
				MetaData->SetValue(NewProp_GravityDirection, TEXT("ToolTip"), TEXT("Whether this actor is currently falling"));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	IMPLEMENT_CLASS(URunebergVR_CustomGravity, 2720844031);
	static FCompiledInDefer Z_CompiledInDefer_UClass_URunebergVR_CustomGravity(Z_Construct_UClass_URunebergVR_CustomGravity, &URunebergVR_CustomGravity::StaticClass, TEXT("/Script/RunebergVRPlugin"), TEXT("URunebergVR_CustomGravity"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(URunebergVR_CustomGravity);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
PRAGMA_ENABLE_OPTIMIZATION
